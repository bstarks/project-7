import junit.framework.TestCase;

import java.io.*;

public class Assign7Test extends TestCase {
	protected int defaultSize = 500;

	public Assign7Test (String name) {
		super(name);
	}


	private void eagerCheck(String name, String answer, String program) {
		eagerCheck(name,answer,program, defaultSize);
	}
	private void eagerCheck(String name, String answer, String program, int hs) {
		Interpreter interp = new Interpreter(new StringReader(program), hs);
		assertEquals("by-value-value " + name, answer, interp.eval().toString());
	}

	private void SDEagerCheck(String name, String answer, String program, int hs) {
		Interpreter interp = new Interpreter(new StringReader(program), hs);
		assertEquals("by-value-value " + name, answer, interp.SDEval().toString());
	}

	private void cpsCheck(String name, String answer, String program, int hs) {
		Interpreter interp = new Interpreter(new StringReader(program), hs);
		assertEquals("by-value-value " + name, answer, interp.cpsEval().toString());
	}

	private void GCSDCpsCheck(String name, String answer, String program, int hs) {
		SDCpsCheck(name,answer,program,hs);
	}

	private void SDCpsCheck(String name, String answer, String program, int hs) {
		//System.out.println("\n----: " + name +"----");
		Interpreter interp = new Interpreter(new StringReader(program), hs);

		int[] heap0 = (int[])interp.getMemory().clone(); //shallow copy works here

		assertEquals("by-value-value " + name, answer, interp.SDCpsEval().toString());
		int[] heap1 = (int[])interp.getMemory().clone();
		int count = 0;
		for (int i=0; i< heap0.length; i++) {
			if (heap0[i] != heap1[i]) { count++; }
		}

		System.out.println("memory size [" + count + "] :" + name);
	}


	private void allCheck(String name, String answer, String program) {
		allCheck(name,answer,program, defaultSize);
	}	
	private void allCheck(String name, String answer, String program, int hs) {
		eagerCheck(name, answer, program, hs);
		cpsCheck(name, answer, program, hs);
		//SDEagerCheck(name, answer, program, hs);
		SDCpsCheck(name, answer, program, hs);
	}

	/*
	  private void nonCPSCheck(String name, String answer, String program, int hs) {
	    eagerCheck(name, answer, program, hs);
//	    SDEagerCheck(name, answer, program, hs);
	  }
	 */
	private void unshadowConvert(String name, String answer, String program) {
		unshadowConvert(name,answer,program, defaultSize);
	}

	private void unshadowConvert(String name, String answer, String program, int hs) {
		Interpreter interp = new Interpreter(new StringReader(program), hs);

		String result = renameVars(interp.unshadow()).toString();
		assertEquals("shadowCheck " + name, answer, result);
	}

	private void cpsConvert(String name, String answer, String program) { 
		cpsConvert(name,answer, program, defaultSize);
	} 

	private void cpsConvert(String name, String answer, String program, int hs) {
		Interpreter interp = new Interpreter(new StringReader(program), hs);

		String result = renameVars(interp.convertToCPS()).toString();
		assertEquals("shadowCheck " + name, answer, result);
	}

	private void sdConvert(String name, String answer, String program) {
		sdConvert(name,answer,program, 50000);
	}

	private void sdConvert(String name, String answer, String program,int hs) {
		Interpreter interp = new Interpreter(new StringReader(program), hs);

		String result = renameVars(interp.convertToSD()).toString();
		assertEquals("shadowCheck " + name, answer, result);
	}

	private AST renameVars(AST tree) { return tree; }



	public void testNumberP() {
		try {
			String output = "number?";
			String input = "number?";
			eagerCheck("numberP", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("numberP threw " + e);
		}
	} //end of func


	public void testMathOp() {
		try {
			String output = "30";
			String input = "2 * 3 + 12";
			allCheck("mathOp", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("mathOp threw " + e);
		}
	} //end of func


	public void testParseException() {
		try {
			String output = "haha";
			String input = " 1 +";
			allCheck("parseException", output, input );

			fail("parseException did not throw ParseException exception");
		} catch (ParseException e) {   
			//e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
			fail("parseException threw " + e);
		}
	} //end of func


	public void testEvalException() {
		try {
			String output = "mojo";
			String input = "1 + number?";
			allCheck("evalException", output, input );

			fail("evalException did not throw EvalException exception");
		} catch (EvalException e) {   
			//e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
			fail("evalException threw " + e);
		}
	} //end of func


	public void testAppend() {
		try {
			String output = "(1 2 3 1 2 3)";
			String input = "let Y    := map f to              let g := map x to f(map z1,z2 to (x(x))(z1,z2));     in g(g);  APPEND := map ap to            map x,y to               if x = null then y else cons(first(x), ap(rest(x), y)); l      := cons(1,cons(2,cons(3,null))); in (Y(APPEND))(l,l)";
			allCheck("append", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("append threw " + e);
		}
	} //end of func


	public void testLetRec() {
		try {
			String output = "(1 2 3 1 2 3)";
			String input = "letrec append :=       map x,y to          if x = null then y else cons(first(x), append(rest(x), y));  in let  l      := cons(1,cons(2,cons(3,null))); in append(l,l)";
			allCheck("letRec", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("letRec threw " + e);
		}
	} //end of func



	public void testEmptyBlock() {
		try {
			String output = "0";
			String input = "{ }";
			allCheck("emptyBlock", output, input );

			fail("emptyBlock did not throw ParseException exception");
		} catch (ParseException e) {   
			//e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
			fail("emptyBlock threw " + e);
		}
	} //end of func


	public void testBlock() {
		try {
			String output = "1";
			String input = "{3; 2; 1}";
			allCheck("block", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("block threw " + e);
		}
	} //end of func


	public void testDupVar() {
		try {
			String output = "ha!";
			String input = "let x:=3; x:=4; in x";
			allCheck("dupVar", output, input );

			fail("dupVar did not throw SyntaxException exception");
		} catch (SyntaxException e) {   
			//e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
			fail("dupVar threw " + e);
		}
	} //end of func


	public void testRefApp() {
		try {
			String output = "(ref 17)";
			String input = "let x := ref 10; in {x <- 17; x}";
			allCheck("refApp", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("refApp threw " + e);
		}
	} //end of func


	public void testRefref() {
		try {
			String output = "(ref (ref 4))";
			String input = "let x:= ref 4; in let y:= ref x; in y";
			allCheck("refref", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("refref threw " + e);
		}
	} //end of func


	public void testBangApp() {
		try {
			String output = "10";
			String input = "let x := ref 10; in !x";
			allCheck("bangApp", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("bangApp threw " + e);
		}
	} //end of func

	public void testUnit(){
		try {
			String input = 
					"let x := ref 1; in {x <- 5}";
			allCheck("unit",JamVoid.ONLY.toString(),input);

		}
		catch(Exception e){
			e.printStackTrace();
			fail("append threw " + e);
		}
	}


	public void testRefEquals1(){
		try {
			String input = 
					"let x := ref 10; y := ref 10; in x = y";
			allCheck("ref1","false",input);

		}
		catch(Exception e){
			e.printStackTrace();
			fail("append threw " + e);
		}
	}

	public void testRefEquals2(){
		try {
			String input = 
					"let x := 5; in let y := ref x; in x = !y";
			allCheck("ref1","true",input);

		}
		catch(Exception e){
			e.printStackTrace();
			fail("append threw " + e);
		}
	}

	public void testRefEquals3(){
		try {
			String input = 
					"let x := ref 5; in let y := ref x; in x = !y";
			allCheck("ref1","true",input);

		}
		catch(Exception e){
			e.printStackTrace();
			fail("append threw " + e);
		}
	}

	public void testIfExpand1(){
		try{
			String input = "true | null?(null)";
			unshadowConvert("shortCircuit","if true then true else null?(null)",input);
		}
		catch (Exception e){
			e.printStackTrace();
			fail("ifExpand1 threw " + e);
		}
	}

	public void testIfExpand2(){
		try{
			String input = "false & null + 1";
			unshadowConvert("shortCircuit","if false then (null + 1) else false",input);
		}
		catch (Exception e){
			e.printStackTrace();
			fail("ifExpand2 threw " + e);
		}
	}

	public void testUuop() {
		try {
			String output = "(3 + 3)";
			String input = "3 + 3";
			unshadowConvert("Uuop", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("Uuop threw " + e);
		}
	} //end of func

	public void testUdeep() {
		try {
			String output = "let x:1 := map x:1 to letrec x:2 := map x:3 to x:3; in x:2(x:2); y:1 := let x:1 := 5; in x:1; in x:1(y:1)";
			String input = "let x:= map x to \n     letrec x:=map x to x; in x(x);\n    y:= let x:=5; in x;\n  in x(y)";
			unshadowConvert("Udeep", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("Udeep threw " + e);
		}
	} //end of func

	public void testUmap() {
		try {
			String output = "map z:1 to z:1";
			String input = "map z to z";
			unshadowConvert("Umap", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("Umap threw " + e);
		}
	} //end of func

	public void testUappend() {
		try {
			String output = "letrec append:1 := map x:2,y:2 to if (x:2 = null) then y:2 else cons(first(x:2), append:1(rest(x:2), y:2)); in let s:2 := cons(1, cons(2, cons(3, null))); in append:1(s:2, s:2)";
			String input = "letrec append := map x,y to\n          if x = null then y else cons(first(x), append(rest\n(x), y));\n            in let s := cons(1,cons(2,cons(3,null)));\n          in append(s,s)";
			unshadowConvert("Uappend", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("Uappend threw " + e);
		}
	} //end of func



	public void testUappend1() {
		try {
			String output = "letrec appendz1:1 := map xz2:2,yz2:2,z0:2 to if (xz2:2 = null) then z0:2(yz2:2) else let z1:3 := first(xz2:2); in appendz1:1(rest(xz2:2), yz2:2, map z3:4 to z0:2(let z2:5 := z3:4; in cons(z1:3, z2:5))); in let sz2:2 := cons(1, cons(2, cons(3, null))); in appendz1:1(sz2:2, sz2:2, map x:3 to x:3)";
			String input = "letrec appendz1 := map xz2,yz2,z0 to if (xz2 =null) then z0(yz2) else let z1 := first(xz2); in appendz1(rest(xz2), yz2, map z3 to z0(let z2 := z3; in cons(z1, z2))); in let sz2 := cons(1, cons(2, cons(3, null))); in appendz1(sz2, sz2, map x to x)";
			unshadowConvert("Uappend1", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("Uappend1 threw " + e);
		}
	} //end of func


	public void testCappend1() {
		try {
			String output = "letrec appendz1:1 := map xz2:2,yz2:2,z0:2,:0 to if (xz2:2 = null) then z0:2(yz2:2, :0) else let z1:3 := first(xz2:2); in appendz1:1(rest(xz2:2), yz2:2, map z3:4,:1 to z0:2(let z2:5 := z3:4; in cons(z1:3, z2:5), :1), :0); in let sz2:2 := cons(1, cons(2, cons(3, null))); in appendz1:1(sz2:2, sz2:2, map x:3,:2 to :2(x:3), map x to x)";
			String input = "letrec appendz1 := map xz2,yz2,z0 to if (xz2 =null) then z0(yz2) else let z1 := first(xz2); in appendz1(rest(xz2), yz2, map z3 to z0(let z2 := z3; in cons(z1, z2))); in let sz2 := cons(1, cons(2, cons(3, null))); in appendz1(sz2, sz2, map x to x)";
			cpsConvert("Cappend1", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("Cappend1 threw " + e);
		}
	} //end of func

	public void testCuop() {
		try {
			String output = "(map x to x)((3 + 3))";
			String input = "3 + 3";
			cpsConvert("Cuop", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("Cuop threw " + e);
		}
	}

	public void testCmap() {
		try {
			String output = "(map x to x)(map z:1,:0 to :0(z:1))";
			String input = "map z to z";
			cpsConvert("Cmap", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("Cmap threw " + e);
		}
	} //end of func

	public void testCarity() {
		try {
			String output = "(map x to x)(map x,k to k((arity(x) - 1)))";
			String input = "arity";
			cpsConvert("Carity", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("Carity threw " + e);
		}
	} //end of func


	public void testCfirst() {
		try {
			String output = "(map x to x)(map x,k to k(first(x)))";
			String input = "first";
			cpsConvert("Cfirst", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("Cfirst threw " + e);
		}
	} //end of func


	public void testCcons() {
		try {
			String output = "(map x to x)(map x,y,k to k(cons(x, y)))";
			String input = "cons";
			cpsConvert("Ccons", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("Ccons threw " + e);
		}
	} //end of func


	public void testClist() {
		try {
			String output = "(map x to x)(first(rest(rest(cons(1, cons(2, cons(3, null)))))))";
			String input = "first(rest(rest(cons(1, cons(2, cons(3, null))))))";
			cpsConvert("Clist", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("Clist threw " + e);
		}
	} //end of func

	public void testCif1(){
		try{
			String input = "if true then (map to 5)() else (map to 6)()";
			String output = "if true then (map x to x)(5) else (map x to x)(6)";
			cpsConvert("Cif1",output,input);
		}catch (Exception e) {
			e.printStackTrace();
			fail("Clist threw " + e);
		}
	}

	public void testCif2(){
		try{
			String input = "if (map to true)() then 5 else 6";
			String output = "5";
			allCheck("Cif2",output,input);
		}catch (Exception e) {
			e.printStackTrace();
			fail("Clist threw " + e);
		}
	}

	public void testCfact() {
		try {
			String output = "720";
			String input = "let Y := map f to let g := map x to f(map z to (x(x))(z)); in g(g);\n         FACT := map f to map n to if n = 0 then 1 else n * f(n - 1);\n      in (Y(FACT))(6)";
			allCheck("Cfact", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("Cfact threw " + e);
		}
	} //end of func

	public void testCfunc(){
		try{
			String output = "5";
			String input = "(map x,y to x(y))(map z to z,5)";
			allCheck("Cfunc",output,input);
		} catch(Exception e){
			e.printStackTrace();
			fail("Cfunc threw " + e);
		}
	}

	public void testSuop() {
		try {
			String output = "(3 + 3)";
			String input = "3 + 3";
			sdConvert("Suop", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("Suop threw " + e);
		}
	} //end of func

	public void testSmap() {
		try {
			String output = "map [*1*] to [0,0]";
			String input = "map z to z";
			sdConvert("Smap", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("Smap threw " + e);
		}
	}

	public void testSdeep() {
		try {
			String output = "let [*2*] map [*1*] to letrec [*1*] map [*1*] to [0,0]; in ([0,0])([0,0]); let [*1*] 5; in [0,0]; in ([0,0])([0,1])";
			String input = "let x:= map x to \n     letrec x:=map x to x; in x(x);\n    y:= let x:=5; in x;\n  in x(y)";
			sdConvert("Sdeep", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("Sdeep threw " + e);
		}
	} //end of func

	public void testSappend() {
		try {
			String output = "letrec [*1*] map [*2*] to if ([0,0] = null) then [0,1] else cons(first([0,0]), ([1,0])(rest([0,0]), [0,1])); in let [*1*] cons(1, cons(2, cons(3, null))); in ([1,0])([0,0], [0,0])";
			String input = "letrec append := map x,y to\n          if x = null then y else cons(first(x), append(rest\n(x), y));\n            in let s := cons(1,cons(2,cons(3,null)));\n          in append(s,s)";
			sdConvert("Sappend", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("Sappend threw " + e);
		}
	} //end of func

	public void testSfact() {
		try {
			String output = "let [*1*] 6; in letrec [*1*] map [*1*] to let [*1*] map [*1*] to ([1,0])(map [*1*] to (([1,0])([1,0]))([0,0])); in ([0,0])([0,0]); in let [*1*] map [*1*] to map [*1*] to if ([0,0] = 0) then 1 else ([0,0] * ([1,0])(([0,0] - 1))); in (([1,0])([0,0]))([2,0])";
			String input = "let n:= 6; in\n   letrec Y := map f to let g := map x to f(map z to (x(x))(z)); in g(g);\n   in \n    let \n       FACT := map f to map n to if n = 0 then 1 else n * f(n - 1);\n      in (Y(FACT))(n)";
			sdConvert("Sfact", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("Sfact threw " + e);
		}
	}

	public void testBadLetrec() {
		try {
			String output = "!";
			String input = "letrec x:=4; in x";
			allCheck("badLetrec", output, input );

			fail("badLetrec did not throw ParseException exception");
		} catch (ParseException e) {   
			//e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
			fail("badLetrec threw " + e);
		}
	} //end of func


	public void testBadLet() {
		try {
			String output = "!";
			String input = "let x:= map z to y(z);\n             y:= map z to x(z); in x(5)";
			allCheck("badLet", output, input );

			fail("badLet did not throw SyntaxException exception");
		} catch (SyntaxException e) {   
			//e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
			fail("badLet threw " + e);
		}
	} //end of func


	public void testFact() {
		try {
			String output = "720";
			String input = "let n:= 6; in\n   letrec Y := map f to let g := map x to f(map z to (x(x))(z)); in g(g);\n   in \n    let \n       FACT := map f to map n to if n = 0 then 1 else n * f(n - 1);\n      in (Y(FACT))(n)";
			allCheck("fact", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("fact threw " + e);
		}
	}

	public void testFactRec(){
		try{
			String output = "720";
			String input = "letrec f := map n to if n = 0 then 1 else n*f(n-1); in f(6)";
			allCheck("factRec", output, input);
		} catch (Exception e) {
			e.printStackTrace();
			fail("fact threw " + e);
		}
	}

	public void testCappend() {
		try {
			String output = "(1 2 3 1 2 3)";
			String input = "letrec append := map x,y to\n          if x = null then y else cons(first(x), append(rest\n(x), y));\n            in let s := cons(1,cons(2,cons(3,null)));\n          in append(s,s)";
			allCheck("Cappend", output, input );

		} catch (Exception e) {
			e.printStackTrace();
			fail("Cappend threw " + e);
		}
	} //end of func

	public void testBigfib() {
		try {
			String output = "((0 1) (1 1) (2 2) (3 3) (4 5) (5 8) (6 13) (7 21) (8 34) (9 55) (10 89) (11 144) (12 233) (13 377) (14 610) (15 987) (16 1597) (17 2584) (18 4181) (19 6765) (20 10946) (21 17711) (22 28657) (23 46368) (24 75025) (25 121393) (26 196418) (27 317811) (28 514229) (29 832040) (30 1346269) (31 2178309) (32 3524578) (33 5702887) (34 9227465) (35 14930352) (36 24157817) (37 39088169) (38 63245986) (39 102334155) (40 165580141))";
			String input = "\nletrec fib :=  map n to if n <= 1 then 1 else fib(n - 1) + fib(n - 2);\n       fibhelp := map k,fn,fnm1 to\n                    if k = 0 then fn\n                    else fibhelp(k - 1, fn + fnm1, fn);\n       pair := map x,y to cons(x, cons(y, null));\nin let ffib := map n to if n = 0 then 1 else fibhelp(n - 1,1,1);\n   in letrec fibs :=  map k,l to \n                        if 0 <= k then \n                        fibs(k - 1, cons(pair(k,ffib(k)), l))\n	                else l;\n      in fibs(40, null)\n";
			allCheck("bigfib", output, input,defaultSize);

		} catch (Exception e) {
			e.printStackTrace();
			fail("bigfib threw " + e);
		}
	} //end of func

}






