/** Nine different interpreters for Jam that differ in binding
 *  policy and cons evaluation policy.
 * 
 *  Binding Policy is either:
 *  call-by-value,
 *  call-by-name, or
 *  call-by-need.
 * 
 *  The cons evaluation policy is either:
 *  call-by-value (eager),
 *  call-by-name (redundant lazy), or
 *  call-by-need (efficient lazy).
 */

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;

/** Interpreter Classes */

/**
 * The exception class for Jam run-time errors.
 */
class EvalException extends RuntimeException {

	private static final long serialVersionUID = 1962023094252778366L;

	/**
	 * Constructor for this exception.
	 * @param msg description
	 */
	EvalException(String msg) {
		super(msg);
	}
}

/**
 * The visitor interface for interpreting ASTs.
 */
interface EvalVisitor extends ASTVisitor<JamVal> {
	/**
	 * Constructs a new visitor of same form with specified environment e.
	 * @param e environment
	 * @return evaluation visitor with the specified environment
	 */
	EvalVisitor newEvalVisitor(PureList<Binding> e);

	/**
	 * Returns the environment embedded in this EvalVisitor.
	 * @return environment
	 */
	PureList<Binding> env();
}

/**
 * The interface supported by various binding evaluation policies: call-by-value, call-by-name, and call-by-need.
 */
interface BindingPolicy {
	/**
	 * Constructs the appropriate binding object for this, binding var to ast in the evaluator ev.
	 * @param var variable
	 * @param ast AST
	 * @param ev evaluation visitor
	 * @return new binding
	 */
	Binding newBinding(Variable var, AST ast, EvalVisitor ev);

	/**
	 * Constructs the appropriate dummy binding object for this.
	 * @param var variable
	 * @return new dummy binding
	 */
	Binding newDummyBinding(Variable var);
}

/**
 * Interface for various cons evaluation policies. A ConsPolicy is typically embedded inside an BindingPolicy.
 */
interface ConsPolicy {
	/**
	 * Constructs the appropriate cons.
	 * @param args arguments
	 * @param ev evaluation visitor
	 * @return cons
	 */
	JamVal evalCons(AST[] args, EvalVisitor ev);
}

/**
 * Interface for classes with a variable field (Variable and the various Binding classes).
 */
interface WithVariable {
	/**
	 * Accessor for the variable.
	 * @return variable
	 */
	Variable var();
}

/**
 * A lookup visitor class that returns element matching the embedded var. If no match found, returns null.
 */
class LookupVisitor<ElemType extends WithVariable> implements PureListVisitor<ElemType, ElemType> {
	/**
	 * Variable to look up.
	 */
	Variable var;

	// the lexer guarantees that there is only one Variable for a given name

	/**
	 * Constructor for a look-up visitor.
	 * @param v variable to look up
	 */
	LookupVisitor(Variable v) {
		var = v;
	}

	/**
	 * Case for empty lists.
	 * @param e host
	 * @return always null
	 */
	public ElemType forEmpty(Empty<ElemType> e) {
		return null;
	}

	/**
	 * Case for non-empty lists.
	 * @param c host
	 * @return the element of the list if it is found, otherwise null
	 */
	public ElemType forCons(Cons<ElemType> c) {
		// System.err.println("forCons in LookUpVisitor invoked; c = " + c);
		ElemType e = c.first();
		if (var.equals(e.var())) {
			return e;
		}
		return c.rest().accept(this);
	}
}

/**
 * Interpreter class.
 */
class Interpreter {
	/**
	 * Parser to use.
	 */
	Parser parser = null;

	/**
	 * Parsed AST.
	 */
	AST prog = null;

	/**
	 * Stack object
	 */
	private Deque<Integer> stack = new ArrayDeque<Integer>();

	/**
	 * Heap array
	 */
	int[] heap = null;

	/**
	 * The last forwarding map used after garbage collection
	 * Needed to repoint pointers in data currently being allocated
	 */
	//HashMap<Integer,Integer> lastFwdPtrMap = new HashMap<Integer,Integer>();

	/**
	 * Size of half of the heap array
	 */
	private int heapHalfSize;

	/**
	 * True if the second half of the heap is the active half;
	 * the half where new objects are allocated
	 */
	private boolean secondHalfActive = false;

	/**
	 * Map array for SD-transformed evaulation
	 */
	MapSD[] sdMaps = null;

	/**
	 * An evaluator for SD-transform programs
	 */
	private SDEvaluator sdVisitor;

	/**
	 * Pointer from where memory allocation starts
	 */
	private int lastPtr;

	/**
	 * The current activation records
	 */
	private PureList<Integer> recordPtrs = new Empty<Integer>();

	/**
	 * Constructor for the interpreter.
	 * @param fileName file name
	 * @throws IOException
	 */
	Interpreter(String fileName, int heapSize) throws IOException {
		heap = new int[heapSize];
		heapHalfSize = heapSize/2;
		parser = new Parser(fileName);
		prog = parser.parseAndCheck();
		sdVisitor = new SDEvaluator(this);
	}

	/**
	 * Constructor for the interpreter.
	 * @param p parser
	 */
	Interpreter(Parser p, int heapSize) {
		heap = new int[heapSize];
		heapHalfSize = heapSize/2;
		parser = p;
		prog = parser.parseAndCheck();
		sdVisitor = new SDEvaluator(this);
	}

	/**
	 * Constructor for the interpreter.
	 * @param reader reader with input
	 */
	Interpreter(Reader reader, int heapSize) {
		heap = new int[heapSize];
		heapHalfSize = heapSize/2;
		parser = new Parser(reader);
		prog = parser.parseAndCheck();
		sdVisitor = new SDEvaluator(this);
	}

	/**
	 * Returns the array used to simulate a heap for jam programs
	 * @return int array representing program heap
	 */
	public int[] getMemory(){
		return heap;
	}

	/**
	 * Push an int-pointer onto the stack
	 */
	public void push(int i){
		stack.addFirst(i);
	}

	/**
	 * Pop an int-pointer off the stackt
	 */
	public int pop(){
		return stack.removeFirst();
	}

	/**
	 * Set the current activate record
	 */
	public void setCurrentRecord(PureList<Integer> records){
		recordPtrs = records;
	}

	/**
	 * Parses and VV interprets the input embeded in parser.
	 * @return result of evaluation
	 */
	public JamVal eval() {
		return prog.accept(valueValueVisitor);
	}

	/**
	 * Performs variable unshadowing on the program
	 */
	public AST unshadow(){
		return prog.accept(new UnshadowingVisitor(0,new Empty<Variable>()));
	}

	/**
	 * Performs a cps transformation on the program
	 */
	public AST convertToCPS(){
		Variable x = new Variable("x");
		Variable[] xArg = {x};
		Map id = new Map(xArg,x);
		AST unshadowed = unshadow();
		return unshadowed.accept(new CPSVisitor(id));
	}

	/**
	 * Evaluates a CPS-transformed version of the program
	 */
	public JamVal cpsEval(){
		AST cps = convertToCPS();
		return cps.accept(valueValueVisitor);
	}

	/**
	 * Perform static distance transformation on the program
	 */
	public AST convertToSD(){
		StaticDistanceVisitor v = new StaticDistanceVisitor(new Empty<Variable[]>());
		AST convProg = unshadow().accept(v);
		sdMaps = v.getMapArray();
		return convProg;
	}

	/**
	 * Evaluates a sd-transformed program
	 */
	public JamVal SDEval(){
		return intPtrToJamVal(convertToSD().accept(sdVisitor));
	}

	/**
	 * Evaluates a CPSed sd-transformed program
	 * @return
	 */
	public JamVal SDCpsEval(){
		StaticDistanceVisitor v = new StaticDistanceVisitor(new Empty<Variable[]>());
		AST newProg = convertToCPS().accept(v);
		sdMaps = v.getMapArray();
		return intPtrToJamVal(newProg.accept(sdVisitor));
	}

	/**
	 * Binding policy for call-by-value.
	 */
	static final BindingPolicy CALL_BY_VALUE = new BindingPolicy() {
		public Binding newBinding(Variable var, AST arg, EvalVisitor ev) {
			return new ValueBinding(var, arg.accept(ev));
		}
		public Binding newDummyBinding(Variable var) {
			return new ValueBinding(var, null);
		}
	};

	/**
	 * Binding policy for call-by-name.
	 */
	static final BindingPolicy CALL_BY_NAME = new BindingPolicy() {
		public Binding newBinding(Variable var, AST arg, EvalVisitor ev) {
			return new NameBinding(var, new Suspension(arg, ev));
		}
		public Binding newDummyBinding(Variable var) {
			return new NameBinding(var, null);
		}
	};

	/**
	 * Binding policy for call-by-need.
	 */
	static final BindingPolicy CALL_BY_NEED = new BindingPolicy() {
		public Binding newBinding(Variable var, AST arg, EvalVisitor ev) {
			return new NeedBinding(var, new Suspension(arg, ev));
		}

		public Binding newDummyBinding(Variable var) {
			return new NeedBinding(var, null);
		}
	};

	/**
	 * Eager cons evaluation policy. presume that args has exactly 2 elements.
	 */
	public static final ConsPolicy EAGER = new ConsPolicy() {
		public JamVal evalCons(AST[] args, EvalVisitor ev) {
			JamVal val0 = args[0].accept(ev);
			JamVal val1 = args[1].accept(ev);
			if (val1 instanceof JamList) {
				return new JamCons(val0, (JamList)val1);
			}
			throw new EvalException("Second argument " + val1 + " to `cons' is not a JamList");
		}
	};

	/**
	 * Call-by-name lazy cons evaluation policy.
	 */
	public static final ConsPolicy LAZYNAME = new ConsPolicy() {
		public JamVal evalCons(AST[] args, EvalVisitor ev) {
			/* System.out.println("LazyNameCons called on " + ToString.toString(args, ", ")); */
			return new JamLazyNameCons(args[0], args[1], ev);
		}
	};

	/**
	 * Call-by-need lazy cons evaluation policy.
	 */
	public static final ConsPolicy LAZYNEED = new ConsPolicy() {
		public JamVal evalCons(AST[] args, EvalVisitor ev) {
			/* System.out.println("LazyNeedCons called on " + ToString.toString(args, ", ")); */
			return new JamLazyNeedCons(args[0], args[1], ev);
		}
	};

	/**
	 * Value-value visitor.
	 */
	static final ASTVisitor<JamVal> valueValueVisitor = new Evaluator(CALL_BY_VALUE, EAGER);

	/**
	 * Value-name visitor.
	 */
	static final ASTVisitor<JamVal> valueNameVisitor = new Evaluator(CALL_BY_VALUE, LAZYNAME);

	/**
	 * Value-need visitor.
	 */
	static final ASTVisitor<JamVal> valueNeedVisitor = new Evaluator(CALL_BY_VALUE, LAZYNEED);

	/**
	 * Name-value visitor.
	 */
	static final ASTVisitor<JamVal> nameValueVisitor = new Evaluator(CALL_BY_NAME, EAGER);

	/**
	 * Name-name visitor.
	 */
	static final ASTVisitor<JamVal> nameNameVisitor = new Evaluator(CALL_BY_NAME, LAZYNAME);

	/**
	 * Name-need visitor.
	 */
	static final ASTVisitor<JamVal> nameNeedVisitor = new Evaluator(CALL_BY_NAME, LAZYNEED);

	/**
	 * Need-value visitor.
	 */
	static final ASTVisitor<JamVal> needValueVisitor = new Evaluator(CALL_BY_NEED, EAGER);

	/**
	 * Need-name visitor.
	 */
	static final ASTVisitor<JamVal> needNameVisitor = new Evaluator(CALL_BY_NEED, LAZYNAME);

	/**
	 * Need-need visitor.
	 */
	static final ASTVisitor<JamVal> needNeedVisitor = new Evaluator(CALL_BY_NEED, LAZYNEED);

	public static final int INT_TAG = 1;
	public static final int BOOL_TAG = 2;
	public static final int REF_TAG = 3;
	public static final int RECORD_TAG = 4;
	public static final int CLOSURE_TAG = 5;
	public static final int CONS_TAG = 6;

	public static final int INT_SIZE = 2;
	public static final int BOOL_SIZE = 2;
	public static final int REF_SIZE = 2;
	public static final int CLOSURE_SIZE = 3;
	public static final int CONS_SIZE = 3;

	/**
	 * JVM entry point. Interpret the specified file.
	 * @param args command line arguments
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String input = "\nletrec fib :=  map n to if n <= 1 then 1 else fib(n - 1) + fib(n - 2);\n       fibhelp := map k,fn,fnm1 to\n                    if k = 0 then fn\n                    else fibhelp(k - 1, fn + fnm1, fn);\n       pair := map x,y to cons(x, cons(y, null));\nin let ffib := map n to if n = 0 then 1 else fibhelp(n - 1,1,1);\n   in letrec fibs :=  map k,l to \n                        if 0 <= k then \n                        fibs(k - 1, cons(pair(k,ffib(k)), l))\n	                else l;\n      in fibs(40, null)\n";
		String goodInput = "let x := 5; in let y := 3; in let z := 4; in let w := 1; in let a := 4; in let b := -15; in 32 - 31 - b + a + w + y + x + z";
		String badInput =  "let x := 5; in let y := 3; in let z := 4; in let w := 1; in let a := 4; in let b := -15; in 32 - 31 - b + a + w + y + x + z + 3";
		Interpreter i = new Interpreter(new StringReader(badInput),100);
		//System.out.println(i.unshadow() + " -> " + i.eval());
		//System.out.println(i.convertToCPS() + " -> " + i.cpsEval());
		System.out.println(i.convertToSD() + " -> " + i.SDEval());

		//		StringBuilder b = new StringBuilder("Memory: [");
		//		int[] memory = i.getMemory();
		//		for(int j = 0; j < memory.length; j++){
		//			b.append(" " + memory[j]);
		//		}
		//		b.append("]");
		//		System.out.println(b.toString());

		//System.out.println(i.convertToCPS().accept(new StaticDistanceVisitor(new Empty<Variable[]>())) + " -> " + i.SDCpsEval());

		StringBuilder b = new StringBuilder("Memory: [");
		int[] memory = i.getMemory();
		for(int j = 0; j < memory.length; j++){
			b.append(" " + memory[j]);
		}
		b.append("]");
		System.out.println(b.toString());

		/**
		System.out.println(i.convertToSD());
		System.out.println(i.convertToCPS().accept(StaticDistanceVisitor.INITIAL));
		System.out.println(i.eval());
		System.out.println(i.sdEval());
		System.out.println(i.sdCpsEval());*/
		/**
        Parser p;
        if (args.length == 0) {
            System.out.println("Usage: java Interpreter <filename> { value | name | need }");
            return;
        }
        p = new Parser(args[0]);
        AST prog = p.parse();
        System.out.println("Parse tree is: " + prog);
		 */
	}

	/**
	 * Allocates space for most data objects
	 * @param tag Tag of data type being allocated
	 * @param data Data of data being allocated
	 * @return int-pointer to allocated object
	 */
	public int allocate(int tag, int data){
		int pointer = 0;
		if(tag == INT_TAG){
			pointer = findFreeSpace(INT_SIZE);
			heap[pointer] = INT_TAG;
			heap[pointer + 1] = data;
		}
		if(tag == BOOL_TAG){
			pointer = findFreeSpace(BOOL_SIZE);
			heap[pointer] = BOOL_TAG;
			heap[pointer + 1] = data;
		}
		if(tag == REF_TAG){
			push(data);
			pointer = findFreeSpace(REF_SIZE);
			heap[pointer] = REF_TAG;
			heap[pointer + 1] = pop();
		}
		return pointer;
	}

	/**
	 * Allocates space for an activation record
	 * @param data Array of int-pointers to objects referred to in the record
	 * @return int-pointer to allocated record
	 */
	public int allocateRecord(int[] data){
		int size = 2 + data.length;

		for(int i = 0; i < data.length; i++){
			push(data[i]);
		}

		int pointer = findFreeSpace(size);
		heap[pointer] = RECORD_TAG;
		heap[pointer + 1] = data.length;
		for(int i = data.length - 1; i >= 0; i--){
			heap[pointer + 2 + i] = pop();
		}
		return pointer;
	}

	/**
	 * Allocates space for a jam closure
	 * @param bodyPtr int-pointer to body of the map (array index of map in sdMaps)
	 * @param records List of activation records (the closure's environment)
	 * @return int-pointer to allocated closure
	 */
	public int allocateClosure(int bodyPtr, PureList<Integer> records){
		Integer[] envTemp = PureListClass.listToArray(records);
		int[] env = new int[envTemp.length];
		for(int i = 0; i < env.length; i++){
			push(envTemp[i]);
		}

		int pointer = findFreeSpace(CLOSURE_SIZE);

		for(int i = env.length - 1; i >= 0; i--){
			env[i] = pop();
		}

		push(pointer);
		int recordPtr = allocateRecord(env);
		pointer = pop();

		heap[pointer] = CLOSURE_TAG;
		heap[pointer + 1] = bodyPtr;
		heap[pointer + 2] = recordPtr;
		return pointer;
	}

	/**
	 * Allocates space for a cons
	 * @param first int-pointer to the first element of the cons
	 * @param rest int-pointer to the rest of the cons, either another cons or null (-1)
	 * @return int-pointer to a cons
	 */
	public Integer allocateCons(int first, int rest) {
		int pointer = findFreeSpace(CONS_SIZE);
		heap[pointer] = CONS_TAG;
		heap[pointer + 1] = first;
		heap[pointer + 2] = rest;
		return pointer;
	}

	private int findFreeSpace(int size) {
		int returnPtr = lastPtr;
		lastPtr += size;
		//Make sure there is enough free space
		if(!inBounds(lastPtr)){
			return garbageCollect(size);
		}
		return returnPtr;
	}

	/**
	 * Returns true if ptr is in the active portion of the heap
	 * @param ptr
	 * @return
	 */
	private boolean inBounds(int ptr) {
		if(secondHalfActive){
			return (heapHalfSize <= ptr) && (ptr < heap.length);
		}
		else{
			return (0 <= ptr) && (ptr < heapHalfSize);
		}
	}

	private int toggleHeapHalf(){
		if(secondHalfActive){
			secondHalfActive = false;
			return 0;
		}
		else{
			secondHalfActive = true;
			return heapHalfSize;
		}
	}

	/**
	 * Try to do garbage collection;  if the garbage collect is successful,
	 * return a pointer to the amount of free space indicated by size
	 * @return
	 */
	private int garbageCollect(int size) {
		lastPtr = toggleHeapHalf();
		final HashMap<Integer,Integer> fwdPtrMap  = new HashMap<Integer,Integer>();
		recordPtrs.accept(new PureListVisitor<Integer,Void>(){ //Repoint current activation record
			public Void forEmpty(Empty<Integer> e) {
				return null;
			}
			public Void forCons(Cons<Integer> c) {
				c.first = repoint(c.first,fwdPtrMap);
				return c.rest.accept(this);
			}
		});

		repointStack(fwdPtrMap); //Repoint temporaries on stack
		purge();

		//Return free space
		int pointer = lastPtr;
		addAndCheck(size);
		return pointer;
	}

	/**
	 * Zeroes out all memory in old space
	 */
	private void purge(){
		int start = secondHalfActive ? 0 : heapHalfSize;
		int finish = secondHalfActive ? heapHalfSize : heap.length;
		for(;start < finish; start++){
			heap[start] = 0;
		}
	}

	private void repointStack(HashMap<Integer, Integer> fwdPtrMap) {
		if(stack.isEmpty()){
			return;
		}

		int top = stack.removeFirst();
		top = repoint(top,fwdPtrMap);
		repointStack(fwdPtrMap);
		stack.addFirst(top);
	}

	/**
	 * Given a ptr to a record in old space, copies the data in old space into
	 * new space, and returns a pointer to new space that points to the same object
	 * that ptr pointed to in old space
	 * @param ptr An int-pointer to an object in old space
	 * @param fwdPtrMap A map that maps already repointed old space pointers to their new space counterpoints
	 * @return A pointer to new space that points to the same object (in new space) that ptr pointed to (in old space)
	 */
	private int repoint(int ptr, HashMap<Integer, Integer> fwdPtrMap) {
		int newPtr = lastPtr;

		//Null and unit still point to the same place
		if(ptr < 0){
			return ptr;
		}

		//If pointer already forwarded, return forwarded pointer
		//Otherwise, forward ptr to newPtr and continue repointing
		if(fwdPtrMap.containsKey(ptr)){
			return fwdPtrMap.get(ptr);
		}
		else{
			fwdPtrMap.put(ptr, newPtr);
		}

		int tag = heap[ptr];
		if(tag == INT_TAG ){
			lastPtr = addAndCheck(INT_SIZE);
			heap[newPtr] = heap[ptr];
			heap[newPtr + 1] = heap[ptr+1];
		}
		if(tag == BOOL_TAG){
			lastPtr = addAndCheck(BOOL_SIZE);
			heap[newPtr] = heap[ptr];
			heap[newPtr + 1] = heap[ptr+1];
		}
		if(tag == REF_TAG){
			lastPtr = addAndCheck(REF_SIZE);
			heap[newPtr] = heap[ptr];
			heap[newPtr + 1] = repoint(heap[ptr+1],fwdPtrMap);
		}
		if(tag == CLOSURE_TAG){
			lastPtr = addAndCheck(CLOSURE_SIZE);
			heap[newPtr] = heap[ptr];
			heap[newPtr + 1] = heap[ptr+1];
			heap[newPtr + 2] = repoint(heap[ptr+2],fwdPtrMap);
		}
		if(tag == CONS_TAG){
			lastPtr = addAndCheck(CONS_SIZE);
			heap[newPtr] = heap[ptr];
			heap[newPtr + 1] = repoint(heap[ptr+1],fwdPtrMap);
			heap[newPtr + 2] = repoint(heap[ptr+2],fwdPtrMap);
		}
		if(tag == RECORD_TAG){
			int size = heap[ptr + 1];
			lastPtr = addAndCheck(2 + size);
			heap[newPtr] = heap[ptr];
			heap[newPtr + 1] = heap[ptr+1];
			for(int i = 0; i < size; i++){
				heap[newPtr + 2 + i] = repoint(heap[ptr+2+i],fwdPtrMap);
			}
		}
		return newPtr;
	}

	/**
	 * Adds size to lastPtr, and checks if it is in bounds
	 * @param size
	 * @return
	 */
	private int addAndCheck(int size) {
		lastPtr += size;
		if(!inBounds(lastPtr)){
			throw new EvalException("Out of memory");
		}
		return lastPtr;
	}

	public JamVal intPtrToJamVal(Integer i) {
		if(i == -1){ //Null case
			return JamEmpty.ONLY;
		}
		if(i == -2){ //Unit case
			return JamVoid.ONLY;
		}

		//For primitive functions
		if(i == -10){
			return FunctionPPrim.ONLY;
		}
		if(i == -11){
			return NumberPPrim.ONLY;
		}
		if(i == -12){
			return ListPPrim.ONLY;
		}
		if(i == -13){
			return ConsPPrim.ONLY;
		}
		if(i == -14){
			return NullPPrim.ONLY;
		}
		if(i == -15){
			return RefPPrim.ONLY;
		}
		if(i == -16){
			return ArityPrim.ONLY;
		}
		if(i == -17){
			return ConsPrim.ONLY;
		}
		if(i == -18){
			return FirstPrim.ONLY;
		}
		if(i == -19){
			return RestPrim.ONLY;
		}

		if(heap[i] == INT_TAG){
			return new IntConstant(heap[i+1]);
		}
		if(heap[i] == BOOL_TAG){
			return heap[i+1] == 1 ? BoolConstant.TRUE : BoolConstant.FALSE;
		}
		if(heap[i] == REF_TAG){
			return new JamBox(intPtrToJamVal(heap[i+1]));
		}
		if(heap[i] == CLOSURE_TAG){
			return getClosure(i);
		}
		if(heap[i] == CONS_TAG){
			return new JamCons(intPtrToJamVal(heap[i+1]), (JamList) intPtrToJamVal(heap[i+2]));
		}
		
		return null;
	}

	public JamClosureSD getClosure(int closurePtr) {
		return new JamClosureSD(sdMaps[heap[closurePtr + 1]],PureListClass.arrayToList(getRecord(heap[closurePtr + 2])));
	}

	private Integer[] getRecord(int ptr) {
		int length = heap[ptr + 1];
		Integer[] record = new Integer[length];
		for(int i = 0; i < length; i++){
			record[i] = heap[ptr + 2 + i];
		}
		return record;
	}

	public Integer getInRecord(int recordPtr, int sdIndex) {
		return heap[recordPtr + 2 + sdIndex];
	}

	public int recordLength(int recordPtr) {
		return heap[recordPtr + 1];
	}

	public void replaceRecord(Integer recordPtr, int[] vals) {
		if(heap[recordPtr + 1] != vals.length){
			throw new EvalException("Replacing record of length " + heap[recordPtr + 1] + " with record of length " + vals.length);
		}
		for(int i = 0; i < vals.length; i++){
			heap[recordPtr + 2 + i] = vals[i];
		}
	}

	/**
	 * Create a new activation record, fixing any pointers into old space
	 * @param record The 'rest' of the new record
	 * @param first The 'first' of the new record
	 * @return The new record with fixed pointers
	 */
	public PureList<Integer> newRecord(PureList<Integer> record, int first) {
		PureList<Integer> newRecord = record.cons(first);
		newRecord.accept(new PureListVisitor<Integer,Void>(){

			@Override
			public Void forEmpty(Empty<Integer> e) {
				return null;
			}

			@Override
			public Void forCons(Cons<Integer> c) {
				return c.rest.accept(this);
			}
		});
		return newRecord;
	}
}

/**
 * Primary visitor class for performing interpretation
 */
class Evaluator implements EvalVisitor {

	// Assumes that:
	//   OpTokens are unique
	//   Variable objects are unique: v1.name.equals(v.name) => v1 == v2
	//   only objects used as boolean values are BoolConstant.TRUE and BoolConstant.FALSE

	// Hence,  == can be used to compare Variable objects, OpTokens, and
	// BoolConstants

	/**
	 * Environment.
	 */
	PureList<Binding> env;

	/**
	 * Policy to create bindings.
	 */
	BindingPolicy bindingPolicy;

	/**
	 * Policy to create cons.
	 */
	ConsPolicy consPolicy;

	/**
	 * Constructor for this evaluation visitor.
	 * @param e environment
	 * @param bp binding policy
	 * @param cp cons policy
	 */
	private Evaluator(PureList<Binding> e, BindingPolicy bp, ConsPolicy cp) {
		env = e;
		bindingPolicy = bp;
		consPolicy = cp;
	}

	/**
	 * Constructor for this evaluation visitor with an empty environment.
	 * @param bp binding policy
	 * @param cp cons policy
	 */
	public Evaluator(BindingPolicy bp, ConsPolicy cp) {
		this(new Empty<Binding>(), bp, cp);
	}

	/* EvalVisitor methods */

	/**
	 * Factory method that constructs a visitor similar to this with environment env
	 * @param env environment for the new visitor.
	 * @return new visitor with the same policies
	 */
	public EvalVisitor newEvalVisitor(PureList<Binding> env) {
		return new Evaluator(env, bindingPolicy, consPolicy);
	}

	/**
	 * Getter for env field.
	 * @return environment
	 */
	public PureList<Binding> env() {
		return env;
	}

	/**
	 * Case for BoolConstants.
	 * @param b host
	 * @return host
	 */
	public JamVal forBoolConstant(BoolConstant b) {
		return b;
	}

	/**
	 * Case for IntConstants.
	 * @param i host
	 * @return host
	 */
	public JamVal forIntConstant(IntConstant i) {
		return i;
	}

	/**
	 * Case for NullConstants.
	 * @param n host
	 * @return empty Jam list
	 */
	public JamVal forNullConstant(NullConstant n) {
		return JamEmpty.ONLY;
	}

	/**
	 * Case for Variables.
	 * @param v host
	 * @return value of variable in current environment
	 */
	public JamVal forVariable(Variable v) {
		Binding match = env.accept(new LookupVisitor<Binding>(v));
		if (match == null) {
			throw new EvalException("variable " + v + " is unbound");
		}
		return match.value();
	}

	/**
	 * Case for PrimFuns.
	 * @param f host
	 * @return host
	 */
	public JamVal forPrimFun(PrimFun f) {
		return f;
	}

	/**
	 * Case for UnOpApps.
	 * @param u host
	 * @return value of unary operator application
	 */
	public JamVal forUnOpApp(UnOpApp u) {
		return u.rator().accept(new UnOpEvaluator(u.arg().accept(this)));
	}

	/**
	 * Case for BinOpApps.
	 * @param b host
	 * @return value of unary operator application
	 */
	public JamVal forBinOpApp(BinOpApp b) {
		return b.rator().accept(new BinOpEvaluator(b.arg1(), b.arg2()));
	}

	/**
	 * Case for Apps.
	 * @param a host
	 * @return value of application
	 */
	public JamVal forApp(App a) {
		JamVal rator = a.rator().accept(this);
		if (rator instanceof JamFun) {
			return ((JamFun)rator).accept(new FunEvaluator(a.args()));
		}
		throw new EvalException(rator + " appears at head of application " + a
				+ " but it is not a valid function");
	}

	/**
	 * Case for Maps.
	 * @param m host
	 * @return closure
	 */
	public JamVal forMap(Map m) {
		return new JamClosure(m, env);
	}

	/**
	 * Case for Ifs.
	 * @param i host
	 * @return value of conditional expression
	 */
	public JamVal forIf(If i) {
		JamVal test = i.test().accept(this);
		if (!(test instanceof BoolConstant)) {
			throw new EvalException("non Boolean " + test + " used as test in if");
		}
		if (test == BoolConstant.TRUE) {
			return i.conseq().accept(this);
		}
		return i.alt().accept(this);
	}

	/**
	 * Case for LetRecs.
	 * @param l host
	 * @return value of body
	 */
	public JamVal forLetRec(LetRec l) {
		/* recursive let semantics */

		/* Extract binding vars and exps (rhs's) from l */
		Variable[] vars = l.vars();
		AST[] exps = l.exps();
		int n = vars.length;

		// construct newEnv for Let body and exps; vars are bound to values of corresponding exps using newEvalVisitor
		PureList<Binding> newEnv = env();
		Binding[] bindings = new Binding[n];
		for(int i = n - 1; i >= 0; i--) {
			bindings[i] = bindingPolicy.newDummyBinding(vars[i]);  // bind var[i] to dummy value null
			newEnv = newEnv.cons(bindings[i]);          // add new Binding to newEnv; it is shared!
		}
		EvalVisitor newEV = newEvalVisitor(newEnv);

		// fix up the dummy values
		for(int i = 0; i < n; i++) {
			bindings[i].setBinding(exps[i], newEV);  // modifies newEnv and newEvalVisitor
		}
		return l.body().accept(newEV);
	}

	/**
	 * Case for Lets.
	 * @param l host
	 * @return value of body
	 */
	public JamVal forLet(Let l) {
		/* non-recursive let semantics */

		/* Extract binding vars and exps (rhs's) from l */
		Variable[] vars = l.vars();
		AST[] exps = l.exps();
		int n = vars.length;

		// construct newEnv for Let body; vars are bound to values of corresponding exps using this visitor, not the new environment
		PureList<Binding> newEnv = env();
		Binding[] bindings = new Binding[n];
		for(int i = n - 1; i >= 0; i--) {
			bindings[i] = bindingPolicy.newBinding(vars[i], exps[i], this);
			newEnv = newEnv.cons(bindings[i]);          // add new Binding to newEnv; it is shared!
		}
		EvalVisitor newEV = newEvalVisitor(newEnv);

		// use newEnv for body only
		return l.body().accept(newEV);
	}

	/* Inner classes */

	/**
	 * Function evaluator.
	 */
	class FunEvaluator implements FunVisitor<JamVal> {
		/**
		 * Unevaluated arguments
		 */
		AST[] args;

		/**
		 * Constructor for the function evaluator.
		 * @param asts unevaluated arguments
		 */
		FunEvaluator(AST[] asts) {
			args = asts;
		}

		/* Support for FunVisitor<JamVal> interface */

		/**
		 * Case for Jam closures.
		 * @param closure closure
		 * @return result of application
		 */
		public JamVal forJamClosure(JamClosure closure) {
			Map map = closure.body();
			int n = args.length;
			Variable[] vars = map.vars();
			if (vars.length != n) {
				throw new EvalException("closure " + closure + " applied to " + n +
						" arguments");
			}

			// construct newEnv for JamClosure body using JamClosure env
			PureList<Binding> newEnv = closure.env();
			for(int i = n - 1; i >= 0; i--) {
				newEnv = newEnv.cons(bindingPolicy.newBinding(vars[i], args[i],Evaluator.this));
			}
			return map.body().accept(newEvalVisitor(newEnv));
		}

		/**
		 * Case for PrimFuns.
		 * @param primFun host
		 * @return result of application
		 */
		public JamVal forPrimFun(PrimFun primFun) {
			return primFun.accept(primEvaluator);
		}

		/**
		 * Evaluator for PrimFuns.
		 */
		PrimFunVisitor<JamVal> primEvaluator = new PrimFunVisitor<JamVal>() {
			/**
			 * Evaluate args using evaluation visitor in whose closure this object is.
			 * @return array of evaluated arguments.
			 */
			private JamVal[] evalArgs() {
				int n = args.length;
				JamVal[] vals = new JamVal[n];
				for(int i = 0; i < n; i++) {
					vals[i] = args[i].accept(Evaluator.this);
				}
				return vals;
			}

			/**
			 * Throw an error.
			 */
			private void primFunError(String fn) {
				throw new EvalException("Primitive function `" + fn + "' applied to " +
						args.length + " arguments");
			}

			/**
			 * Evaluate an argument that has to be a Jam cons.
			 * @param arg argument
			 * @param fun function performing this evaluation
			 * @return Jam cons
			 */
			private JamCons evalJamConsArg(AST arg, String fun) {
				JamVal val = arg.accept(Evaluator.this);
				if (val instanceof JamCons) {
					return (JamCons)val;
				}
				throw new EvalException("Primitive function `" + fun + "' applied to argument " + val +
						" that is not a JamCons");
			}

			/**
			 * Case for function?
			 *
			 * @return BoolConstant.TRUE if argument evaluates to a JamFun
			 */
			public JamVal forFunctionPPrim() {
				JamVal[] vals = evalArgs();
				if (vals.length != 1) {
					primFunError("function?");
				}
				return BoolConstant.toBoolConstant(vals[0] instanceof JamFun);
			}

			/**
			 * Case for number?
			 *
			 * @return BoolConstant.TRUE if argument evaluates to a IntConstant
			 */
			public JamVal forNumberPPrim() {
				JamVal[] vals = evalArgs();
				if (vals.length != 1) {
					primFunError("number?");
				}
				return BoolConstant.toBoolConstant(vals[0] instanceof IntConstant);
			}

			/**
			 * Case for list?
			 *
			 * @return BoolConstant.TRUE if argument evaluates to a JamList
			 */
			public JamVal forListPPrim() {
				JamVal[] vals = evalArgs();
				if (vals.length != 1) {
					primFunError("list?");
				}
				return BoolConstant.toBoolConstant(vals[0] instanceof JamList);
			}

			/**
			 * Case for cons?
			 *
			 * @return BoolConstant.TRUE if argument evaluates to a JamCons
			 */
			public JamVal forConsPPrim() {
				JamVal[] vals = evalArgs();
				if (vals.length != 1) {
					primFunError("cons?");
				}
				return BoolConstant.toBoolConstant(vals[0] instanceof JamCons);
			}

			/**
			 * Case for null?
			 *
			 * @return BoolConstant.TRUE if argument evaluates to a JamEmpty
			 */
			public JamVal forNullPPrim() {
				JamVal[] vals = evalArgs();
				if (vals.length != 1) {
					primFunError("null?");
				}
				return BoolConstant.toBoolConstant(vals[0] instanceof JamEmpty);
			}

			/**
			 * Case for cons
			 *
			 * @return JamCons
			 */
			public JamVal forConsPrim() {
				if (args.length != 2) {
					primFunError("cons");
				}
				return consPolicy.evalCons(args, Evaluator.this);  /* Evaluation strategy determined by consEp */
			}

			/**
			 * Case for arity
			 * @return IntConstant representing the arity of the argument
			 */
			public JamVal forArityPrim() {
				JamVal[] vals = evalArgs();
				if (vals.length != 1) {
					primFunError("arity");
				}
				if (!(vals[0] instanceof JamFun)) {
					throw new EvalException("arity applied to argument " +
							vals[0]);
				}
				return ((JamFun)vals[0]).accept(new FunVisitor<IntConstant>() {
					public IntConstant forJamClosure(JamClosure jc) {
						return new IntConstant(jc.body().vars().length);
					}

					public IntConstant forPrimFun(PrimFun jpf) {
						return new IntConstant(jpf.accept(ArityVisitor.ONLY));
					}
				});
			}

			/**
			 * Case for first
			 * @return first
			 */
			public JamVal forFirstPrim() {
				if (args.length != 1) {
					primFunError("first");
				}
				return evalJamConsArg(args[0], "first").first();
			}

			/**
			 * Case for rest
			 * @return rest
			 */
			public JamVal forRestPrim() {
				if (args.length != 1) {
					primFunError("rest");
				}
				return evalJamConsArg(args[0], "rest").rest();
			}


			public JamVal forRefPPrim() {
				if (args.length != 1) {
					primFunError("ref");
				}
				return BoolConstant.toBoolConstant(args[0].accept(Evaluator.this) instanceof JamBox);
			}
		};
	}

	/**
	 * Evaluator for unary operators.
	 */
	static class UnOpEvaluator implements UnOpVisitor<JamVal> {
		/**
		 * Value of the operand.
		 */
		private JamVal val;

		/**
		 * Constructor for this evaluator.
		 * @param jv value of the operand
		 */
		UnOpEvaluator(JamVal jv) {
			val = jv;
		}

		/**
		 * Return the value of the operand if it is an IntConstant, otherwise throw an exception,
		 * @param op operator the value gets applied to
		 * @return value of the operand, if it is an IntConstant
		 */
		private IntConstant checkInteger(UnOp op) {
			if (val instanceof IntConstant) {
				return (IntConstant)val;
			}
			throw new EvalException("Unary operator `" + op + "' applied to non-integer " + val);
		}

		/**
		 * Return the value of the operand if it is a BoolConstant, otherwise throw an exception,
		 * @param op operator the value gets applied to
		 * @return value of the operand, if it is an BoolConstant
		 */
		private BoolConstant checkBoolean(UnOp op) {
			if (val instanceof BoolConstant) {
				return (BoolConstant)val;
			}
			throw new EvalException("Unary operator `" + op + "' applied to non-boolean " + val);
		}

		private JamBox checkBox(UnOp op){
			if (val instanceof JamBox){
				return (JamBox)val;
			}
			throw new EvalException("Unary operator '" + op + "' applied to non-reference value " + val);
		}

		/**
		 * Case for unary plus.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forUnOpPlus(UnOpPlus op) {
			return checkInteger(op);
		}

		/**
		 * Case for unary minus.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forUnOpMinus(UnOpMinus op) {
			return new IntConstant(-checkInteger(op).value());
		}

		/**
		 * Case for not.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpTilde(OpTilde op) {
			return checkBoolean(op).not();
		}

		@Override
		public JamVal forOpBang(OpBang op) {
			return checkBox(op).value();
		}

		@Override
		public JamVal forOpRef(OpRef op) {
			return new JamBox(val);
		}
	}

	/**
	 * Evaluator for binary operators.
	 */
	class BinOpEvaluator implements BinOpVisitor<JamVal> {
		/**
		 * Unevaluated arguments.
		 */
		private AST arg1, arg2;

		/**
		 * Constructor for this evaluator
		 * @param a1 unevaluted first argument
		 * @param a2 unevaluated second argument
		 */
		BinOpEvaluator(AST a1, AST a2) {
			arg1 = a1;
			arg2 = a2;
		}

		/**
		 * Return the value of the argument if it is an IntConstant, otherwise throw an exception,
		 * @param arg argument to evaluate and check
		 * @param b operator the value gets applied to
		 * @return value of the argument, if it is an IntConstant
		 */
		private IntConstant evalIntegerArg(AST arg, BinOp b) {
			JamVal val = arg.accept(Evaluator.this);
			if (val instanceof IntConstant) {
				return (IntConstant)val;
			}
			throw new EvalException("Binary operator `" + b + "' applied to non-integer " + val);
		}

		/**
		 * Return the value of the argument if it is a BoolConstant, otherwise throw an exception,
		 * @param arg argument to evaluate and check
		 * @param b operator the value gets applied to
		 * @return value of the argument, if it is a BoolConstant
		 */
		private BoolConstant evalBooleanArg(AST arg, BinOp b) {
			JamVal val = arg.accept(Evaluator.this);
			if (val instanceof BoolConstant) {
				return (BoolConstant)val;
			}
			throw new EvalException("Binary operator `" + b + "' applied to non-boolean " + val);
		}

		private JamBox evalJamBoxArg(AST arg, BinOp b){
			JamVal val = arg.accept(Evaluator.this);
			if (val instanceof JamBox){
				return (JamBox)val;
			}
			throw new EvalException("Binary operator `" + b + "' applied to non-reference value " + val);
		}

		/**
		 * Case for binary plus.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forBinOpPlus(BinOpPlus op) {
			return new IntConstant(evalIntegerArg(arg1, op).value() + evalIntegerArg(arg2, op).value());
		}

		/**
		 * Case for binary minus.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forBinOpMinus(BinOpMinus op) {
			return new IntConstant(evalIntegerArg(arg1, op).value() - evalIntegerArg(arg2, op).value());
		}

		/**
		 * Case for times.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpTimes(OpTimes op) {
			return new IntConstant(evalIntegerArg(arg1, op).value() * evalIntegerArg(arg2, op).value());
		}

		/**
		 * Case for divide.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpDivide(OpDivide op) {
			int divisor = evalIntegerArg(arg2, op).value();
			if (divisor == 0) {
				throw new EvalException("Attempt to divide by zero");
			}
			return new IntConstant(evalIntegerArg(arg1, op).value() / divisor);
		}

		/**
		 * Case for equals.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpEquals(OpEquals op) {
			return BoolConstant.toBoolConstant(arg1.accept(Evaluator.this).equals(arg2.accept(Evaluator.this)));
		}

		/**
		 * Case for not equals.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpNotEquals(OpNotEquals op) {
			return BoolConstant.toBoolConstant(!arg1.accept(Evaluator.this).equals(arg2.accept(Evaluator.this)));
		}

		/**
		 * Case for less than.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpLessThan(OpLessThan op) {
			return BoolConstant.toBoolConstant(evalIntegerArg(arg1, op).value() < evalIntegerArg(arg2, op).value());
		}

		/**
		 * Case for greater than.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpGreaterThan(OpGreaterThan op) {
			return BoolConstant.toBoolConstant(evalIntegerArg(arg1, op).value() > evalIntegerArg(arg2, op).value());
		}

		/**
		 * Case for less than or equals.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpLessThanEquals(OpLessThanEquals op) {
			return BoolConstant.toBoolConstant(evalIntegerArg(arg1, op).value() <= evalIntegerArg(arg2, op).value());
		}

		/**
		 * Case for greater than or equals.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpGreaterThanEquals(OpGreaterThanEquals op) {
			return BoolConstant.toBoolConstant(evalIntegerArg(arg1, op).value() >= evalIntegerArg(arg2, op).value());
		}

		/**
		 * Case for and.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpAnd(OpAnd op) {
			BoolConstant b1 = evalBooleanArg(arg1, op);
			if (b1 == BoolConstant.FALSE) {
				return BoolConstant.FALSE;
			}
			return evalBooleanArg(arg2, op);
		}

		/**
		 * Case for or.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpOr(OpOr op) {
			BoolConstant b1 = evalBooleanArg(arg1, op);
			if (b1 == BoolConstant.TRUE) {
				return BoolConstant.TRUE;
			}
			return evalBooleanArg(arg2, op);
		}

		@Override
		public JamVal forOpGets(OpGets op) {
			JamBox v1 = evalJamBoxArg(arg1,op);
			JamVal v2 = arg2.accept(Evaluator.this);
			v1.mutate(v2);
			return JamVoid.ONLY;
		}
	}

	@Override
	public JamVal forBlock(Block b) {
		JamVal last = null;
		for(AST exp : b.exps){
			last = exp.accept(this);
		}
		return last;
	}
}

/**
 * An evaluator for programs converted into a static-distance format
 *
 */
//class SDEvaluator extends Evaluator{
//	PureList<JamVal[]> sdEnv;
//	Interpreter interp;
//	
//
//	/**
//	 * Creates a new SD-transformed program evaulatior
//	 * @param i The interpreter that is using this class
//	 */
//	public SDEvaluator(Interpreter i){
//		super(Interpreter.CALL_BY_VALUE, Interpreter.EAGER);
//		sdEnv = new Empty<JamVal[]>();
//		interp = i;
//	}
//
//	public SDEvaluator(PureList<JamVal[]> newEnv, Interpreter i){
//		super(Interpreter.CALL_BY_VALUE, Interpreter.EAGER);
//		sdEnv = newEnv;
//		interp = i;
//	}
//	
//
//	public JamVal forVariable(Variable v){
//		final int[] depth = {0};
//		final VariableSD vsd = (VariableSD)v;
//		JamVal val = sdEnv.accept(new PureListVisitor<JamVal[],JamVal>(){
//			public JamVal forEmpty(Empty<JamVal[]> e) {
//				return null;
//			}
//
//			public JamVal forCons(Cons<JamVal[]> c) {
//				if(vsd.sdDepth() == depth[0]){
//					JamVal[] vals = c.first();
//					if(vsd.sdIndex() < vals.length){
//						return vals[vsd.sdIndex()];
//					}
//					else{
//						return null;
//					}
//				}
//				else{
//					depth[0] += 1;
//					return c.rest().accept(this);
//				}
//			}
//
//		});
//		if(val != null){
//			return val;
//		}
//		else{
//			throw new EvalException(v + " is not a valid variable");
//		}
//	}
//
//	public JamVal forLet(Let l){
//		PureList<JamVal[]> newEnv = sdEnv;
//		newEnv = newEnv.cons(evalAll(l.exps()));
//		return l.body().accept(new SDEvaluator(newEnv,interp));
//	}
//
//	public JamVal forLetRec(LetRec l){
//		//Create a dummy frame, evaluate the rhs, and replace the
//		//dummy frame with the evaluated frame
//		AST[] args = l.exps();
//		JamVal[] frame = new JamVal[args.length];
//		JamVal[] vals = new JamVal[args.length];
//		for(int i = 0; i < vals.length; i++){
//			frame[i] = null;
//		}
//		
//		PureList<JamVal[]> newEnv = sdEnv.cons(frame);
//		for(int i = 0; i < vals.length; i++){
//			vals[i] = args[i].accept(new SDEvaluator(newEnv,interp));
//		}
//		((Cons<JamVal[]>)newEnv).first = vals;
//		return l.body().accept(new SDEvaluator(newEnv,interp));
//	}
//
//	public JamVal forApp(App a){
//		JamVal rator = a.rator().accept(this);
//		if (rator instanceof JamFun) {
//			return ((JamFun)rator).accept(new FunSDEvaluator(a.args()));
//		}
//		throw new EvalException(rator + " appears at head of application " + a
//				+ " but it is not a valid function");
//	}
//
//	public JamVal forMap(Map m){
//		return new JamClosureSD((MapSD)m,sdEnv);
//	}
//
//	private JamVal[] evalAll(AST[] args){
//		JamVal[] vals = new JamVal[args.length];
//		for(int i = 0; i < vals.length; i++){
//			vals[i] = args[i].accept(this);
//		}
//		return vals;
//	}
//
//	class FunSDEvaluator extends FunEvaluator{
//		FunSDEvaluator(AST[] asts) {
//			super(asts);
//		}
//
//		public JamVal forJamClosure(JamClosure closure) {
//			MapSD map = interp.sdMaps[((JamClosureSD)closure).mapIndex()];
//			int n = args.length;
//			VariableSD[] vars = (VariableSD[]) ((MapSD)map).vars();
//			if (vars.length != n) {
//				throw new EvalException("closure " + closure + " applied to " + n +
//						" arguments");
//			}
//
//			PureList<JamVal[]> newEnv = ((JamClosureSD)closure).sdEnv();
//			newEnv = newEnv.cons(evalAll(args));
//			return map.body().accept(new SDEvaluator(newEnv,interp));
//		}
//	}
//}

/**
 * An evaluator for programs converted into a static-distance format
 * Each method returns an int, a 'pointer' into the heap of the
 * interpreter using this evaluator
 * */
class SDEvaluator implements ASTVisitor<Integer>{
	private Interpreter interp;

	/**
	 * A list of integer pointers to allocation records
	 */
	PureList<Integer> records;

	/**
	 * int-pointer to current activation record
	 */
	int recordPtr;

	public SDEvaluator(Interpreter i){
		interp = i;
		records = new Empty<Integer>(); //no records yet
		recordPtr = -1; //points to null
		interp.setCurrentRecord(records);
	}

	public SDEvaluator(PureList<Integer> env, Interpreter i){
		interp = i;
		records = env;
		recordPtr = records.accept(new PureListVisitor<Integer,Integer>(){
			public Integer forEmpty(Empty<Integer> e) {
				return -1;
			}

			public Integer forCons(Cons<Integer> c) {
				return c.first;
			}		
		});
		interp.setCurrentRecord(records);
	}

	@Override
	public Integer forIntConstant(IntConstant i) {
		int pointer = interp.allocate(Interpreter.INT_TAG, i.value());
		return pointer;
	}

	@Override
	public Integer forBoolConstant(BoolConstant b) {
		int pointer = interp.allocate(Interpreter.BOOL_TAG, b.value() ? 1 : 0);
		return pointer;
	}

	@Override
	public Integer forNullConstant(NullConstant n) {
		return -1;
	}

	@Override
	public Integer forVariable(Variable v) {
		final int[] depth = {0};
		final VariableSD vsd = (VariableSD)v;
		Integer ptr = records.accept(new PureListVisitor<Integer,Integer>(){
			public Integer forEmpty(Empty<Integer> e) {
				return null;
			}

			public Integer forCons(Cons<Integer> c) {
				if(vsd.sdDepth() == depth[0]){
					int recordPtr = c.first;
					if(vsd.sdIndex() < interp.recordLength(recordPtr)){
						return interp.getInRecord(recordPtr,vsd.sdIndex());
					}
					else{
						return null;
					}
				}
				else{
					depth[0] += 1;
					return c.rest().accept(this);
				}
			}

		});
		if(ptr != null){
			return ptr;
		}
		else{
			throw new EvalException(v + " is not a valid variable");
		}
	}

	@Override
	public Integer forPrimFun(PrimFun f) {
		return f.accept(new PrimFunVisitor<Integer>(){
			public Integer forFunctionPPrim() {
				return -10;
			}

			@Override
			public Integer forNumberPPrim() {
				return -11;
			}

			@Override
			public Integer forListPPrim() {
				return -12;
			}

			@Override
			public Integer forConsPPrim() {
				return -13;
			}

			@Override
			public Integer forNullPPrim() {
				return -14;
			}

			@Override
			public Integer forRefPPrim() {
				return -15;
			}

			@Override
			public Integer forArityPrim() {
				return -16;
			}

			@Override
			public Integer forConsPrim() {
				return -17;
			}

			@Override
			public Integer forFirstPrim() {
				return -18;
			}

			@Override
			public Integer forRestPrim() {
				return -19;
			}
		});
	}

	@Override
	public Integer forUnOpApp(UnOpApp u) {
		return u.rator().accept(new UnOpEvaluatorSD(u.arg().accept(this)));
	}

	@Override
	public Integer forBinOpApp(BinOpApp b) {
		interp.push( b.arg1().accept(this));
		interp.push( b.arg2().accept(this));
		int arg1 = interp.pop();
		int arg2 = interp.pop();
		return b.rator().accept(new BinOpEvaluatorSD(arg1,arg2));
	}

	@Override
	public Integer forApp(App a) {
		int rator = a.rator().accept(this);
		if(a.rator() instanceof PrimFun){
			return ((PrimFun)a.rator()).accept(new FunEvaluatorSD(a.args()));
		}
		if(rator >= 0 && interp.heap[rator] == Interpreter.CLOSURE_TAG){
			JamClosureSD closure = interp.getClosure(rator);
			return closure.accept(new FunEvaluatorSD(a.args()));
		}

		throw new EvalException(a.rator() + " appears at head of application " + a + " but it is not a valid function");
		/**
		 * JamVal rator = a.rator().accept(this);
		if (rator instanceof JamFun) {
			return ((JamFun)rator).accept(new FunEvaluator(a.args()));
		}
		throw new EvalException(rator + " appears at head of application " + a
				+ " but it is not a valid function");
		 */
	}

	public Integer forMap(Map m){
		return interp.allocateClosure(((MapSD)m).index(), records);
	}

	@Override
	public Integer forIf(If i) {
		int boolPtr = i.test().accept(this);
		if (interp.heap[boolPtr] != Interpreter.BOOL_TAG) {
			throw new EvalException("non Boolean " + i.test() + " used as test in if");
		}
		int boolValue = interp.heap[boolPtr+1];
		if (boolValue == 1) {
			return i.conseq().accept(this);
		}
		return i.alt().accept(this);
	}

	@Override
	public Integer forLet(Let l) {
		/* non-recursive let semantics */

		/* Extract binding vars and exps (rhs's) from l */
		AST[] exps = l.exps();

		// construct new record for Let body; vars are bound to values of corresponding exps using this visitor, not the new environment
		PureList<Integer> newRecord = records;
		pushToStack(newRecord);
		newRecord = interp.newRecord(newRecord,interp.allocateRecord(evalAll(exps)));
		popFromStack(((Cons<Integer>)newRecord).rest);


		// use newEnv for body only
		return l.body().accept(new SDEvaluator(newRecord,interp));
	}

	@Override
	public Integer forLetRec(LetRec l) {
		//Create a dummy record, evaluate the rhs, and replace the
		//dummy record pointers with the evaluated pointers
		AST[] exps = l.exps();
		int[] vals = new int[exps.length];
		int[] frame = new int[exps.length];
		for(int i = 0; i < vals.length; i++){
			frame[i] = -3;
		}

		PureList<Integer> newEnv = records;
		newEnv = interp.newRecord(newEnv,interp.allocateRecord(frame));
		for(int i = 0; i < vals.length; i++){
			interp.push(exps[i].accept(new SDEvaluator(newEnv,interp)));
		}
		for(int i = vals.length - 1; i >= 0; i--){
			vals[i] = interp.pop();
		}
		interp.replaceRecord(((Cons<Integer>)newEnv).first,vals);
		return l.body().accept(new SDEvaluator(newEnv,interp));
	}

	@Override
	public Integer forBlock(Block b) {
		int last = 0;
		for(AST exp : b.exps){
			last = exp.accept(this);
		}
		return last;
	}

	private int[] evalAll(AST[] args) {
		int[] retArgs = new int[args.length];
		for(int i = 0; i < args.length; i++){
			interp.push(args[i].accept(SDEvaluator.this));
		}
		for(int i = args.length - 1; i >= 0; i--){
			retArgs[i] = interp.pop();
		}
		return retArgs;
	}
	
	private void popFromStack(PureList<Integer> newEnv) {
		newEnv.accept(new PureListVisitor<Integer,Void>(){
			public Void forEmpty(Empty<Integer> e) {
				return null;
			}

			@Override
			public Void forCons(Cons<Integer> c) {
				c.first = interp.pop();
				return c.rest.accept(this);
			}
		});
	}

	private void pushToStack(PureList<Integer> newEnv) {
		final Integer[] elems = PureListClass.listToArray(newEnv);
		final int[] i = {elems.length - 1};
		newEnv.accept(new PureListVisitor<Integer,Void>(){
			public Void forEmpty(Empty<Integer> e) {
				return null;
			}

			@Override
			public Void forCons(Cons<Integer> c) {
				interp.push(elems[i[0]]);
				i[0] -= 1;
				return c.rest.accept(this);
			}

		});
	}

	private class FunEvaluatorSD implements FunVisitor<Integer>{
		private AST[] args;

		public FunEvaluatorSD(AST[] asts){
			args = asts;
		}

		public Integer forJamClosure(JamClosure closure) {
			MapSD map = interp.sdMaps[((JamClosureSD)closure).mapIndex()];
			int n = args.length;
			VariableSD[] vars = (VariableSD[]) ((MapSD)map).vars();
			if (vars.length != n) {
				throw new EvalException("closure " + closure + " applied to " + n +
						" arguments");
			}

			PureList<Integer> newEnv = ((JamClosureSD)closure).sdEnv();
			pushToStack(newEnv);
			newEnv = interp.newRecord(newEnv,interp.allocateRecord(evalAll(args)));
			popFromStack(((Cons<Integer>)newEnv).rest);
			return map.body().accept(new SDEvaluator(newEnv,interp));
		}

		@Override
		public Integer forPrimFun(PrimFun pf) {
			return pf.accept(primEvaluatorSD);
		}

		PrimFunVisitor<Integer> primEvaluatorSD = new PrimFunVisitor<Integer>(){
			private int allocateBool(boolean b){
				return interp.allocate(Interpreter.BOOL_TAG, b ? 1 : 0);
			}

			private int[] evalArgs(){
				int[] ptrs = new int[args.length];
				for(int i = 0; i < ptrs.length; i++){
					interp.push(args[i].accept(SDEvaluator.this));
				}
				for(int i = ptrs.length - 1; i >= 0; i--){
					ptrs[i] = interp.pop();
				}
				return ptrs;
			}

			public Integer forFunctionPPrim() {
				int ptr = evalArgs()[0];
				if((ptr >= 0 && interp.heap[ptr] == Interpreter.CLOSURE_TAG) || args[0] instanceof PrimFun){
					return allocateBool(true);
				}
				else{
					return allocateBool(false);
				}
			}

			@Override
			public Integer forNumberPPrim() {
				int ptr = evalArgs()[0];
				return allocateBool(ptr >= 0 && interp.heap[ptr] == Interpreter.INT_TAG);
			}

			@Override
			public Integer forListPPrim() {
				int ptr = evalArgs()[0];
				return allocateBool(ptr != -1 && interp.heap[ptr] == Interpreter.CONS_TAG);
			}

			@Override
			public Integer forConsPPrim() {
				int ptr = evalArgs()[0];
				return allocateBool(ptr >= 0 && interp.heap[ptr] == Interpreter.CONS_TAG);
			}

			@Override
			public Integer forNullPPrim() {
				int ptr = evalArgs()[0];
				return allocateBool(ptr == -1);
			}

			@Override
			public Integer forRefPPrim() {
				int ptr = evalArgs()[0];
				return allocateBool(ptr >= 0 && interp.heap[ptr] == Interpreter.REF_TAG);
			}

			@Override
			public Integer forArityPrim() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Integer forConsPrim() {
				int[] data = evalArgs();
				return interp.allocateCons(data[0],data[1]);
			}

			@Override
			public Integer forFirstPrim() {
				int ptr = evalArgs()[0];
				return interp.heap[ptr+1];
			}

			@Override
			public Integer forRestPrim() {
				int ptr = evalArgs()[0];
				return interp.heap[ptr+2];
			}

		};

	}
	//		FunSDEvaluator(AST[] asts) {
	//			super(asts);
	//		}
	//
	//		public JamVal forJamClosure(JamClosure closure) {
	//			MapSD map = interp.sdMaps[((JamClosureSD)closure).mapIndex()];
	//			int n = args.length;
	//			VariableSD[] vars = (VariableSD[]) ((MapSD)map).vars();
	//			if (vars.length != n) {
	//				throw new EvalException("closure " + closure + " applied to " + n +
	//						" arguments");
	//			}
	//
	//			PureList<JamVal[]> newEnv = ((JamClosureSD)closure).sdEnv();
	//			newEnv = newEnv.cons(evalAll(args));
	//			return map.body().accept(new SDEvaluator(newEnv,interp));
	//		}
	//	}

	private class UnOpEvaluatorSD implements UnOpVisitor<Integer>{
		/**
		 * Pointer to UnOp's argument in the heap
		 */
		private int pointer;

		public UnOpEvaluatorSD(int p){
			pointer = p;
		}
		public Integer forUnOpPlus(UnOpPlus op) {
			return interp.allocate(Interpreter.INT_TAG, +interp.heap[pointer]);
		}

		@Override
		public Integer forUnOpMinus(UnOpMinus op) {
			return interp.allocate(Interpreter.INT_TAG, -interp.heap[pointer]);
		}

		@Override
		public Integer forOpTilde(OpTilde op) {
			BoolConstant b = (BoolConstant) interp.intPtrToJamVal(pointer);
			return interp.allocate(Interpreter.BOOL_TAG, b.value() ? 0 : 1);
		}

		@Override
		public Integer forOpBang(OpBang op) {
			return interp.heap[pointer+1];
		}

		@Override
		public Integer forOpRef(OpRef op) {
			return interp.allocate(Interpreter.REF_TAG, pointer);
		}

	}

	private class BinOpEvaluatorSD implements BinOpVisitor<Integer>{
		/**
		 * Int pointers to BinOp args in heap
		 */
		int ptr1;
		int ptr2;

		public BinOpEvaluatorSD(int p1, int p2){
			ptr1 = p1;
			ptr2 = p2;
		}

		@Override
		public Integer forBinOpPlus(BinOpPlus op) {
			JamVal v1 = interp.intPtrToJamVal(ptr1);
			JamVal v2 = interp.intPtrToJamVal(ptr2);
			IntConstant i1 = checkInt(v1,op);
			IntConstant i2 = checkInt(v2,op);
			return interp.allocate(Interpreter.INT_TAG, i1.value() + i2.value());
		}

		@Override
		public Integer forBinOpMinus(BinOpMinus op) {
			JamVal v1 = interp.intPtrToJamVal(ptr1);
			JamVal v2 = interp.intPtrToJamVal(ptr2);
			IntConstant i1 = checkInt(v1, op);
			IntConstant i2 = checkInt(v2, op);
			return interp.allocate(Interpreter.INT_TAG, i1.value() - i2.value());
		}

		@Override
		public Integer forOpTimes(OpTimes op) {
			JamVal v1 = interp.intPtrToJamVal(ptr1);
			JamVal v2 = interp.intPtrToJamVal(ptr2);
			IntConstant i1 = checkInt(v1, op);
			IntConstant i2 = checkInt(v2, op);
			return interp.allocate(Interpreter.INT_TAG, i1.value() * i2.value());
		}

		@Override
		public Integer forOpDivide(OpDivide op) {
			JamVal v1 = interp.intPtrToJamVal(ptr1);
			JamVal v2 = interp.intPtrToJamVal(ptr2);
			IntConstant i1 = checkInt(v1,op);
			IntConstant i2 = checkInt(v2,op);
			return interp.allocate(Interpreter.INT_TAG, i1.value() / i2.value());
		}

		@Override
		public Integer forOpEquals(OpEquals op) {
			JamVal v1 = interp.intPtrToJamVal(ptr1);
			JamVal v2 = interp.intPtrToJamVal(ptr2);
			if(v1 instanceof JamFun || v1 instanceof JamBox){
				return interp.allocate(Interpreter.BOOL_TAG,ptr1 == ptr2 ? 1 : 0);
			}
			return interp.allocate(Interpreter.BOOL_TAG, v1.equals(v2) ? 1 : 0);
		}

		@Override
		public Integer forOpNotEquals(OpNotEquals op) {
			JamVal v1 = interp.intPtrToJamVal(ptr1);
			JamVal v2 = interp.intPtrToJamVal(ptr2);
			return interp.allocate(Interpreter.BOOL_TAG, v1.equals(v2) ? 0 : 1);
		}

		@Override
		public Integer forOpLessThan(OpLessThan op) {
			JamVal v1 = interp.intPtrToJamVal(ptr1);
			JamVal v2 = interp.intPtrToJamVal(ptr2);
			IntConstant i1 = checkInt(v1,op);
			IntConstant i2 = checkInt(v2,op);
			return interp.allocate(Interpreter.BOOL_TAG, (i1.value() < i2.value()) ? 1 : 0);
		}

		@Override
		public Integer forOpGreaterThan(OpGreaterThan op) {
			JamVal v1 = interp.intPtrToJamVal(ptr1);
			JamVal v2 = interp.intPtrToJamVal(ptr2);
			IntConstant i1 = checkInt(v1,op);
			IntConstant i2 = checkInt(v2,op);
			return interp.allocate(Interpreter.BOOL_TAG, (i1.value() > i2.value()) ? 1 : 0);
		}

		@Override
		public Integer forOpLessThanEquals(OpLessThanEquals op) {
			JamVal v1 = interp.intPtrToJamVal(ptr1);
			JamVal v2 = interp.intPtrToJamVal(ptr2);
			IntConstant i1 = checkInt(v1,op);
			IntConstant i2 = checkInt(v2,op);
			return interp.allocate(Interpreter.BOOL_TAG, (i1.value() <= i2.value()) ? 1 : 0);
		}

		@Override
		public Integer forOpGreaterThanEquals(OpGreaterThanEquals op) {
			JamVal v1 = interp.intPtrToJamVal(ptr1);
			JamVal v2 = interp.intPtrToJamVal(ptr2);
			IntConstant i1 = checkInt(v1,op);
			IntConstant i2 = checkInt(v2,op);
			return interp.allocate(Interpreter.BOOL_TAG, (i1.value() >= i2.value()) ? 1 : 0);
		}

		@Override
		public Integer forOpAnd(OpAnd op) {
			JamVal v1 = interp.intPtrToJamVal(ptr1);
			JamVal v2 = interp.intPtrToJamVal(ptr2);
			BoolConstant b1 = checkBool(v1,op);
			BoolConstant b2 = checkBool(v2,op);
			return interp.allocate(Interpreter.BOOL_TAG, (b1.value() && b2.value()) ? 1 : 0);
		}

		@Override
		public Integer forOpOr(OpOr op) {
			JamVal v1 = interp.intPtrToJamVal(ptr1);
			JamVal v2 = interp.intPtrToJamVal(ptr2);
			BoolConstant b1 = checkBool(v1,op);
			BoolConstant b2 = checkBool(v2,op);
			return interp.allocate(Interpreter.BOOL_TAG,(b1.value() || b2.value()) ? 1 : 0);
		}

		@Override
		public Integer forOpGets(OpGets op) {
			interp.heap[ptr1 + 1] = ptr2;
			return -2; //return Unit
		}

		private IntConstant checkInt(JamVal v1, BinOp op) {
			if(v1 instanceof IntConstant){
				return (IntConstant) v1;
			}
			else{
				throw new EvalException("Binary operator '" + op + "' applied to non-integer " + v1);
			}
		}

		private BoolConstant checkBool(JamVal v, BinOp op){
			if(v instanceof BoolConstant){
				return (BoolConstant) v;
			}
			else{
				throw new EvalException("Binary operator '" + op + "' applied to non-bool " + v);
			}
		}

	}

}


class ArityVisitor implements PrimFunVisitor<Integer>{
	public static final ArityVisitor ONLY = new ArityVisitor();
	private ArityVisitor(){}

	public Integer forFunctionPPrim() {
		return 1;
	}

	public Integer forNumberPPrim() {
		return 1;
	}

	public Integer forListPPrim() {
		return 1;
	}

	public Integer forConsPPrim() {
		return 1;
	}

	public Integer forNullPPrim() {
		return 1;
	}

	public Integer forArityPrim() {
		return 1;
	}

	public Integer forConsPrim() {
		return 2;
	}

	public Integer forFirstPrim() {
		return 1;
	}

	public Integer forRestPrim() {
		return 1;
	}

	@Override
	public Integer forRefPPrim() {
		return 1;
	}
}


