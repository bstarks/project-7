import java.util.ArrayList;




class CPSVisitor implements ASTVisitor<AST> {
	private AST k;
	private VarNamer vName;

	public CPSVisitor(AST continuation){
		k = continuation;
		vName = new VarNamer();
	}

	public CPSVisitor(AST continuation,VarNamer nextVarName){
		k = continuation;
		vName = nextVarName;
	}
	/**
	 * Constants are simple jam expressions
	 * Cps(k,M) = k(Rsh(M)) = k(M)
	 */
	public AST forBoolConstant(BoolConstant b) {
		return makeNewApp(b);
	}

	public AST forIntConstant(IntConstant i) {
		return makeNewApp(i);
	}

	public AST forNullConstant(NullConstant n) {
		return makeNewApp(n);
	}

	/**
	 * Variables and functions are simple jam expressions
	 * Cps(k,M) = k(Rsh(M))
	 */
	public AST forVariable(Variable v) {
		return makeNewApp(v);
	}

	public AST forPrimFun(PrimFun f) {
		return makeNewApp(f.accept(new ReshapeVisitor()));
	}

	/**
	 * UnOpApps and BinOpApps can be treated as primitive applications
	 */
	public AST forUnOpApp(UnOpApp u) {
		//If S is simple
		//Cps(k,S) = k(Rsh(S))
		if(u.accept(SimpleExpressionVisitor.ONLY)){
			return makeNewApp(u.accept(new ReshapeVisitor()));
		}
		else if(u.arg().accept(SimpleExpressionVisitor.ONLY)){
			//Cps[k, S(S1)] = Rsh[S](Rsh[S1], k) = (map x,k to k(S(x)))(Rsh[S1],k)
			Variable x = new Variable("x");
			Variable k = new Variable("k");
			AST[] appArgs = {new UnOpApp(u.rator(),x)};
			Variable[] mapVars = {x,k};
			App mapBody = new App(k,appArgs);
			Map m = new Map(mapVars,mapBody);

			AST[] outerAppArgs = {u.arg().accept(new ReshapeVisitor()), k};
			return new App(m,outerAppArgs);
		}
		else{
			Variable v = makeNewVariable();
			Variable[] letVar = {v};
			AST[] letExp = {u.arg()};
			AST letBody = new UnOpApp(u.rator(),v);
			Let l = makeNewLet(letVar,letExp,letBody);
			return l.accept(new CPSVisitor(k,vName));
		}
	}

	public AST forBinOpApp(BinOpApp b) {
		//If S is simple
		//Cps(k,S) = k(Rsh(S))
		if(b.accept(SimpleExpressionVisitor.ONLY)){
			return makeNewApp(b.accept(new ReshapeVisitor()));
		}
		else if(b.arg1().accept(SimpleExpressionVisitor.ONLY) && b.arg2().accept(SimpleExpressionVisitor.ONLY)){
			//Cps[k, S(S1,S2)] = Rsh[S](Rsh[S1], Rsh[S2], k) = (map x,y,k to k(S(x,y)))(Rsh[S1], Rsh[S2], k)
			Variable x = new Variable("x");
			Variable y = new Variable("y");
			Variable k = new Variable("k");
			AST[] appArgs = {new BinOpApp(b.rator(),x,y)};
			Variable[] mapVars = {x,y,k};
			App mapBody = new App(k,appArgs);
			Map m = new Map(mapVars,mapBody);

			AST[] outerAppArgs = {b.arg1().accept(new ReshapeVisitor()), b.arg2().accept(new ReshapeVisitor()), k};
			return new App(m,outerAppArgs);
		}
		else{
			Variable v1 = makeNewVariable();
			Variable v2 = makeNewVariable();
			Variable[] letVar = {v1,v2};
			AST[] letExp = {b.arg1(),b.arg2()};
			AST letBody = new BinOpApp(b.rator(),v1,v2);
			Let l = makeNewLet(letVar,letExp,letBody);
			return l.accept(new CPSVisitor(k,vName));
		}
	}

	public AST forApp(App a) {
		//If S is simple
		//Cps(k,S) = k(Rsh(S))
		if(a.accept(SimpleExpressionVisitor.ONLY)){
			return makeNewApp(a.accept(new ReshapeVisitor()));
		}
		//If the rator of a is a map then the following rules apply:
		//Cps[k, (map x1, ..., xn to B)(E1, ...,En)] = Cps[k, let x1 :=E1; ...; xn :=En; in B]
		//Cps[k, (map to B)()] = Cps[k, B]
		if(a.rator() instanceof Map){
			Map m = (Map)a.rator();
			AST[] args = a.args();
			if(args.length == 0){
				return m.body().accept(new CPSVisitor(k,vName));
			}
			else{
				Let l = makeNewLet(m.vars(),args,m.body());
				return l.accept(new CPSVisitor(k,vName));
			}
		}
		//If the rator of a is some other simple expression, the following rules apply:
		//Cps[k, S(S1, ..., Sn)] = Rsh[S](Rsh[S1], ..., Rsh[Sn], k)
		//Cps[k, S(E1, ...,En)] = Cps[k, let v1 :=E1; ... vn :=En; in S(v1, ..., vn)]
		if(a.rator().accept(SimpleExpressionVisitor.ONLY)){
			AST[] args = a.args();
			if(allSimple(args)){
				AST rshS = a.rator().accept(new ReshapeVisitor());
				AST[] rshArgs = reshapeAll(args);
				AST[] newArgs = new AST[args.length + 1];
				for(int i = 0; i < args.length; i++){
					newArgs[i] = rshArgs[i];
				}
				newArgs[newArgs.length - 1] = k;
				return new App(rshS,newArgs);
			}
			else{
				Variable[] vars = new Variable[args.length];
				for(int i = 0; i < vars.length; i++){
					vars[i] = makeNewVariable();
				}
				App sv = new App(a.rator(),vars);
				Let l = makeNewLet(vars,args,sv);
				return l.accept(new CPSVisitor(k,vName));
			}
		}
		//Finally, if the rator of a is not simple, apply this rule:
		//Cps[k, B(E1, ...,En)] = Cps[k, let v :=B; v1 :=E1; ... vn :=En; in v(v1, ..., vn)]
		else{
			AST[] args = a.args();
			Variable[] letVars = new Variable[args.length+1];
			Variable[] vArgs = new Variable[args.length];
			AST[] letArgs = new AST[args.length+1];
			letVars[0] = makeNewVariable();
			letArgs[0] = a.rator();
			for(int i = 1; i < letVars.length; i++){
				letVars[i] = makeNewVariable();
				letArgs[i] = args[i-1];
				vArgs[i-1] = letVars[i];
			}
			App vToVArgs = new App(letVars[0],vArgs);
			Let l = makeNewLet(letVars,letArgs,vToVArgs);
			return l.accept(new CPSVisitor(k,vName));
		}
	}

	public AST forMap(Map m) {
		return makeNewApp(m.accept(new ReshapeVisitor()));
	}

	public AST forIf(If i) {
		//If S is simple
		//Cps(k,S) = k(Rsh(S))
		if(i.accept(SimpleExpressionVisitor.ONLY)){
			return makeNewApp(i.accept(new ReshapeVisitor()));
		}
		AST test = i.test();
		AST conseq = i.conseq();
		AST alt = i.alt();
		//If the test clause is a simple expression
		//Cps[k, if S then A else C] = if Rsh[S] then Cps[k, A] else Cps[k, C]
		if(test.accept(SimpleExpressionVisitor.ONLY)){
			return new If(test.accept(new ReshapeVisitor()),conseq.accept(new CPSVisitor(k,vName)),alt.accept(new CPSVisitor(k,vName)));
		}
		//If the test clause is not a simple expression
		//Cps[k, if T then A else C] ) Cps[k, let v := T in if v then A else C]
		else{
			Variable v = makeNewVariable();
			If body = new If(v,conseq,alt);
			Variable[] letVar = {v};
			AST[] letExp = {test};
			return makeNewLet(letVar,letExp,body).accept(new CPSVisitor(k,vName));
		}
	}

	public AST forLet(Let l) {
		Variable[] vars = l.vars();
		AST[] exps = l.exps();
		//System.out.println(l);

		//Reduce "let in B" to just "B"
		if(exps.length == 0){
			return l.body().accept(new CPSVisitor(k,vName));
		}
		//If the first m expressions of the let are simple, use this rule:
		//Cps[k, let x1 :=S1; ... ; xm := Sm; xm+1 :=Em+1; ... ; xn :=En; in B] = let x1 :=Rsh[S1]; ... ; xm := Rsh[Sm]; in Cps[k, let xm+1 :=Em+1; ... ; xn :=En; in B]
		if(exps[0].accept(SimpleExpressionVisitor.ONLY)){
			int i = 0;
			ArrayList<AST> rshExps = new ArrayList<AST>();
			ArrayList<AST> othExps = new ArrayList<AST>();
			ArrayList<Variable> rshVars = new ArrayList<Variable>();
			ArrayList<Variable> othVars = new ArrayList<Variable>();
			//Separate first m simple expressions and the other expressions into different lists
			for(i = 0; i < exps.length; i++){
				AST exp = exps[i];
				if(exp.accept(SimpleExpressionVisitor.ONLY)){
					rshExps.add(exp.accept(new ReshapeVisitor()));
					rshVars.add(vars[i]);
				}
				else{
					break;
				}
			}
			for(;i < exps.length; i++){
				othExps.add(exps[i]);
				othVars.add(vars[i]);
			}
			Let innerLet = makeNewLet(othVars.toArray(new Variable[0]), othExps.toArray(new AST[0]), l.body());
			return makeNewLet(rshVars.toArray(new Variable[0]), rshExps.toArray(new AST[0]), innerLet.accept(new CPSVisitor(k,vName)));

		}
		//If the first expression of the let is not simple, then this rule applies:
		//Cps[k, let x1 :=E1; ... xn :=En; in B] = Cps[map v to Cps[k, let x1 := v; ... xn :=En; in B], E1]
		//Cps[k, let x1 :=E1; ... xn :=En; in B]=  Cps[map x1 to Cps[k, let x2 :=E2; ... ; xn :=En; in B], E1]
		else{
			Variable[] mapArg = {vars[0]};
			AST[] letExps = new AST[exps.length - 1];
			Variable[] newVars = new Variable[vars.length - 1];
			for(int i = 0; i < letExps.length; i++){
				letExps[i] = exps[i+1];
				newVars[i] = vars[i+1];
			}
			Let innerLet = makeNewLet(newVars,letExps,l.body());
			AST newContinuation = new Map(mapArg,innerLet.accept(new CPSVisitor(k,vName)));
			return exps[0].accept(new CPSVisitor(newContinuation,vName));
		}
	}

	public AST forLetRec(LetRec l) {
		//If S is simple
		//Cps(k,S) = k(Rsh(S))
		if(l.accept(SimpleExpressionVisitor.ONLY)){
			return makeNewApp(l.accept(new ReshapeVisitor()));
		}
		//Otherwise
		//Cps[k, letrec p1 := map ... to E1; ...; pn := map ... to En; in B] = letrec p1 := Rsh[map... to E1]; ...; pn := Rsh[map... to En]; in Cps[k,B]
		AST[] exps = l.exps();
		AST[] letExps = new AST[exps.length];
		for(int i = 0; i < exps.length; i++){
			letExps[i] = exps[i].accept(new ReshapeVisitor());
		}
		return makeNewLetRec(l.vars(),letExps,l.body().accept(new CPSVisitor(k,vName)));
	}

	public AST forBlock(Block b) {
		if(b.accept(SimpleExpressionVisitor.ONLY)){
			return makeNewApp(b.accept(new ReshapeVisitor()));
		}
		Variable[] vars = new Variable[b.exps.length];
		for(int i = 0; i < vars.length; i++){
			vars[i] = makeNewVariable();
		}
		Let l = makeNewLet(vars,b.exps,vars[vars.length - 1]);
		return l.accept(this);
	}

	private App makeNewApp(AST ast){
		AST[] arg = {ast};
		return new App(k,arg);
	}

	/**
	 * Creates a new variable whose name is ":k",
	 * k starts at 0 and is incremented each time a variable
	 * is created, so that each new variable has a unique name
	 * @return
	 */
	private Variable makeNewVariable(){
		Variable v = new Variable(vName.getVarName());
		return v;
	}

	private Let makeNewLet(Variable[] vars, AST[] args, AST body) {
		int length = vars.length < args.length ? vars.length : args.length;
		Def[] defs = new Def[length];
		for(int i = 0; i < length; i++){
			defs[i] = new Def(vars[i],args[i]);
		}
		return new Let(defs,body);
	}

	private LetRec makeNewLetRec(Variable[] vars, AST[] args, AST body) {
		int length = vars.length < args.length ? vars.length : args.length;
		Def[] defs = new Def[length];
		for(int i = 0; i < length; i++){
			defs[i] = new Def(vars[i],args[i]);
		}
		return new LetRec(defs,body);
	}

	private boolean allSimple(AST[] args){
		for(AST ast : args){
			if (ast.accept(SimpleExpressionVisitor.ONLY) != true){
				return false;
			}
		}
		return true;
	}

	private AST[] reshapeAll(AST[] args){
		AST[] rshArgs = new AST[args.length];
		for(int i = 0; i < args.length; i++){
			rshArgs[i] = args[i].accept(new ReshapeVisitor());
		}
		return rshArgs;
	}

	/**
	 * Helper class for CPSVisitor
	 * A wrapper around an integer used to name generated variables
	 */
	private class VarNamer{
		private int num = -1;
		public String getVarName(){
			num++;
			return ":"+num;
		}
	}

	/**
	 * Helper visitor for CPSVisitor
	 * This visitor needs its host to be a simple expression
	 * @author Billy
	 *
	 */
	private class ReshapeVisitor implements ASTVisitor<AST>{
		public AST forBoolConstant(BoolConstant b) {
			return b;
		}

		public AST forIntConstant(IntConstant i) {
			return i;
		}

		public AST forNullConstant(NullConstant n) {
			return n;
		}

		public AST forVariable(Variable v) {
			return v;
		}

		@Override
		public AST forPrimFun(PrimFun f) {
			Variable x = new Variable("x");
			Variable k = new Variable("k");
			//Rsh[arity] = map x,k to k(arity(x) - 1)
			if(f == ArityPrim.ONLY){
				Variable[] xArg = {x};
				Variable[] vars = {x,k};
				App fx = new App(ArityPrim.ONLY,xArg);
				BinOpApp minus1 = new BinOpApp(BinOpMinus.ONLY,fx,new IntConstant(1));
				AST[] kxArg = {minus1};
				App kx = new App(k,kxArg);
				return new Map(vars,kx);
			}
			//Rsh[f] = map x,k to k(f(x))
			else if(f.accept(ArityVisitor.ONLY) == 1){
				Variable[] xArg = {x};
				Variable[] vars = {x,k};
				App fx = new App(f,xArg);
				App[] kArg = {fx};
				return new Map(vars,new App(k,kArg));
			}
			//Rsh[g] = map x,y,k to k(g(x,y))
			else{
				Variable y = new Variable("y");
				Variable[] vars = {x,y,k};
				Variable[] xyArgs = {x,y};
				App gx = new App(f,xyArgs);
				App[] kArg = {gx};
				return new Map(vars,new App(k,kArg));
			}
		}

		//UnOpApps are treated as unary primitive applications
		//Rsh[f(S1)] = f(Rsh[S1])
		public AST forUnOpApp(UnOpApp u) {
			return new UnOpApp(u.rator(),u.arg().accept(this));
		}

		//BinOpApps are treated as binary primitive applications
		//Rsh[f(S1,S2)] = f(Rsh[S1],Rsh[S2])
		public AST forBinOpApp(BinOpApp b) {
			return new BinOpApp(b.rator(),b.arg1().accept(this),b.arg2().accept(this));
		}

		@Override
		public AST forApp(App a) {
			//Rsh[arity(S)] = arity(Rsh[S]) - 1;
			if(a.rator() == ArityPrim.ONLY){
				AST rshS = a.args()[0].accept(this);
				AST[] args = {rshS};
				App arityRshS = new App(ArityPrim.ONLY,args);
				return new BinOpApp(BinOpMinus.ONLY,arityRshS,new IntConstant(1));
			}
			//Rsh[f(S1, ..., Sn)] = f(Rsh[S1], ..., Rsh[Sn])
			else{
				AST[] oldArgs = a.args();
				AST[] args = new AST[oldArgs.length];
				for(int i = 0; i < args.length; i++){
					args[i] = oldArgs[i].accept(this);
				}
				return new App(a.rator(),args);
			}
		}

		@Override
		public AST forMap(Map m) {
			//Rsh[map x1, ..., xn to E] ) map x1, ..., xn, v toCps[v, E]
			Variable[] oldVars = m.vars();
			Variable[] vars = new Variable[oldVars.length+1];
			for(int i = 0; i < oldVars.length; i++){
				vars[i] = oldVars[i];
			}
			Variable v = makeNewVariable();
			vars[vars.length - 1] = v;
			return new Map(vars,m.body().accept(new CPSVisitor(v,vName)));
		}

		public AST forIf(If i) {
			//Rsh[if S1 then S2 else S3] = if Rsh[S1] then Rsh[S2] else Rsh[S3]
			return new If(i.test().accept(this),i.conseq().accept(this),i.alt().accept(this));
		}

		@Override
		public AST forLet(Let l) {
			//Rsh[let x1 := S1; ...; xn := Sn; in S] = let x1 :=Rsh[S1]; ...; xn :=Rsh[Sn]; in Rsh[S]
			AST[] exps = l.exps();
			Variable[] vars = l.vars();
			Def[] defs = new Def[exps.length];
			for(int i = 0; i < defs.length; i++){
				defs[i] = new Def(vars[i],exps[i].accept(this));
			}
			return new Let(defs,l.body().accept(this));
		}

		@Override
		public AST forLetRec(LetRec l) {
			//Rsh[letrec p1 := map ... to E1; ...; pn := map ... to En; in S] = letrec p1 := Rsh[map... toE1]; ...; pn := Rsh[map... toEn]; in Rsh[S]
			AST[] exps = l.exps();
			Variable[] vars = l.vars();
			Def[] defs = new Def[exps.length];
			for(int i = 0; i < defs.length; i++){
				defs[i] = new Def(vars[i],exps[i].accept(this));
			}
			return new LetRec(defs,l.body().accept(this));
		}

		public AST forBlock(Block b) {
			//Rsh[{S1; ...; Sn}] = {Rsh[S1]; ...; Rsh[Sn-1]; Rsh[Sn]}
			AST[] exps = new AST[b.exps.length];
			for(int i = 0; i < exps.length; i++){
				exps[i] = b.exps[i].accept(this);
			}
			return new Block(exps);
		}
	}
}



/**
 * Helper visitor for CPSVisitor
 * Checks if an expression is simple or not (if all of its applications are primitive)
 */
class SimpleExpressionVisitor implements ASTVisitor<Boolean>{
	public static final SimpleExpressionVisitor ONLY = new SimpleExpressionVisitor();
	private SimpleExpressionVisitor(){}

	public Boolean forBoolConstant(BoolConstant b) {
		return true;
	}

	@Override
	public Boolean forIntConstant(IntConstant i) {
		return true;
	}

	@Override
	public Boolean forNullConstant(NullConstant n) {
		return true;
	}

	@Override
	public Boolean forVariable(Variable v) {
		return true;
	}

	@Override
	public Boolean forPrimFun(PrimFun f) {
		return true;
	}

	@Override
	public Boolean forUnOpApp(UnOpApp u) {
		return u.arg().accept(this);
	}

	@Override
	public Boolean forBinOpApp(BinOpApp b) {
		return (b.arg1().accept(this)) && (b.arg2().accept(this));
	}

	@Override
	public Boolean forApp(App a) {
		if (a.rator() instanceof PrimFun){
			for(AST ast : a.args()){
				if(ast.accept(this) == false){
					return false;
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public Boolean forMap(Map m) {
		return true;
	}

	@Override
	public Boolean forIf(If i) {
		return (i.test().accept(this)) && (i.conseq().accept(this)) && (i.alt().accept(this));
	}

	@Override
	public Boolean forLet(Let l) {
		for(Def def : l.defs){
			if(def.rhs().accept(this) != true){
				return false;
			}
		}
		return l.body().accept(this);
	}

	@Override
	public Boolean forLetRec(LetRec l) {
		for(Def def : l.defs){
			if(def.rhs().accept(this) != true){
				return false;
			}
		}
		return l.body().accept(this);
	}

	@Override
	public Boolean forBlock(Block b) {;
	for(AST exp : b.exps){
		if(exp.accept(this) != true){
			return false;
		}
	}
	return true;
	}

}
